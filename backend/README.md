
# Table of Contents

1.  [The environment file](#orgd3ddab2)
2.  [To create a new SQL-Lite database](#orgcbccc87)
3.  [Jatos](#org29795ba)

**NOTE** strongly advised to use a virtual environment, like [Poetry](https://python-poetry.org/) or [venv](https://docs.python.org/3/library/venv.html). I
 personally used [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/). Once you have created a virtual environment,
 install the dependencies listed in `requirements.txt`


<a id="orgd3ddab2"></a>

# The environment file

The project assumes you keep a `.env` file in the backend's root directory
(the one where this readme lives). The environment file defines:

-   The **environment type** `ENV`. This is a string, which can currently be one of
    `[dev, test, prod]`. This is used when multiple databases exist in the
    backend's root; when the backend's configuration is read during startup,
    this string is prefixed to the key used for the database's url, and the
    correct database is thus selected. **NOTE** you can also do the following:

        export ENV=dev
        python run.py

-   The **Redis database URL** `AIOREDIS_URL`
-   The **experiments configuration file** `JATOS_CONFIG_PATH`. The corresponding
    file defines the field of an experiment that we want to relay to our
    database from the Jatos workers.
-   The **database path** `{ENV}_DB_URL`

**NOTE** one problem with this setup is that the Docker version of the app also
follows this convention, and that the values for the above variables must be
changed when working with Docker to match the container's filesystem. One
simple solution would be to store these variables in a file named differently
for Docker, e.g. `.env.docker`.


<a id="orgcbccc87"></a>

# To create a new SQL-Lite database

Once you set the `{ENV}_DB_URL` environment variable:

    ./prestart.sh

This command will use [Alembic](https://alembic.sqlalchemy.org/en/latest/) to create the database tables and run the
migrations if any exist.
Alternatively you can use

    ./prestart.sh t

to enrich the database with the entities defined in `app/populate.py`, to e.g.
add the admins, teachers etc. The environment variables relevant to the
database are also read from the `.env` file, as can be seen in
`app/migrations/env.py`.


<a id="org29795ba"></a>

# Jatos

**NOTE** on Arch Linux, the Jatos version I have been working with (3.6.1) works
 with `java-11-openjdk` and it fails with `java-17-openjdk`. If you use this
 version of Jatos, make sure to use Java11.
