import uuid
from typing import Union

from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from ..database import crud
from ..database import models as db_m
from ..database import utils as db_u


def unauthorized_error():
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Incorrect username or password",
    )


def permission_error():
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="You do not have permissions to see this",
    )


def gen_session_token() -> str:
    return str(uuid.uuid4())


def authenticate_registered_user(
    email: str, password: str, sess: Session
) -> Union[db_m.Teacher, db_m.Admin, bool]:
    user = crud.get_user_by_email(sess, email)
    return (
        False
        if not (user and db_u.verify_password(password, user.password_hash))
        else user
    )


def authenticate_student(code: str, sess: Session) -> db_m.Student:
    return crud.get_student(sess, code)
