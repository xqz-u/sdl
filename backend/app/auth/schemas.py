from pydantic import BaseModel


class StatusResponse(BaseModel):
    status: int


class LoginResponse(StatusResponse):
    role: str
