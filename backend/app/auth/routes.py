from fastapi import APIRouter, Depends, Form, Response
from sqlalchemy.orm import Session

from .. import dependencies as deps
from ..database import utils as db_u
from . import schemas as auth_sch
from . import utils as auth_u

router = APIRouter(prefix="/auth", tags=["Auth"])


# NOTE the two login routes could be unified to avoid code duplication
# TODO csrf protection
# TODO expiry time?
@router.post(
    "/login",
    summary="Login route for Admins and Teachers",
    response_model=auth_sch.LoginResponse,
)
async def login_registered(
    response: Response,
    username: str = Form(...),
    password: str = Form(...),
    sess: Session = Depends(deps.database_session),
):
    # check if user exists in persistent storage
    user_db = auth_u.authenticate_registered_user(username, password, sess)
    if not user_db:
        return auth_u.unauthorized_error()
    # generate new access token, attach it to the author of the request
    sess_tok = auth_u.gen_session_token()
    user_db.access_token = sess_tok
    db_u.modify_session_entry(sess, user_db)
    response.set_cookie("Authorization", value=sess_tok, httponly=True)
    return {"role": user_db.type, "status": 200}


@router.post(
    "/login-student",
    summary="Login route for Students",
    response_model=auth_sch.LoginResponse,
)
async def login_student(
    response: Response,
    code: str = Form(...),
    username: str = Form(...),
    sess: Session = Depends(deps.database_session),
):
    # check if user exists in persistent storage
    student_db = auth_u.authenticate_student(code, sess)
    if not student_db:
        return auth_u.unauthorized_error()
    student_db.username = username
    # generate new access token, attach it to the author of the request
    sess_tok = auth_u.gen_session_token()
    student_db.access_token = sess_tok
    db_u.modify_session_entry(sess, student_db)
    response.set_cookie("Authorization", value=sess_tok, httponly=True)
    return {"role": student_db.type, "status": 200}


@router.delete("/logout", response_model=auth_sch.StatusResponse)
async def logout(
    res: Response, user_sess: tuple = Depends(deps.get_active_user_and_session)
):
    user_db, sess = user_sess
    user_db.access_token = ""
    db_u.modify_session_entry(sess, user_db)
    res.delete_cookie("Authorization")
    return {"status": 200}
