from typing import Tuple

from fastapi import Cookie, Depends
from sqlalchemy.orm import Session

from .auth import utils as auth_u
from .database import crud
from .database import models as db_m
from .database import schemas as db_sch
from .database import utils as db_u
from .user import utils as user_u


# used in route dependencies
def database_session():
    with db_u.Transaction() as sess:
        yield sess


def get_active_user_and_session(
    Authorization: str = Cookie(""), sess: Session = Depends(database_session)
) -> Tuple[db_m.User, Session]:
    user_db = crud.get_auth_user(sess, Authorization)
    if not user_db:
        return auth_u.permission_error()
    return user_db, sess


def get_admin_and_session(
    user_db_sess: Tuple[db_m.User, Session] = Depends(get_active_user_and_session)
) -> Tuple[db_m.Admin, Session]:
    # check if user is admin
    if not user_u.is_admin(user_db_sess[0]):
        return auth_u.permission_error()
    return user_db_sess


def get_user_schema(
    user_db_sess: Tuple[db_m.User, Session] = Depends(get_active_user_and_session),
) -> db_sch.DBUser:
    user_db, _ = user_db_sess
    return db_sch.make_user_schema(user_db)
