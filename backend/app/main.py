from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import config as conf

# from . import utils as u
from .admin import routes as adm_routes
from .auth import routes as auth_routes
from .user import routes as user_routes
from .user.results import routes as results_routes
from .ws_redis import endpoint as ws_redis_routes
from .ws_redis import utils as redis_u

# NOTE FastAPI knows how to run functions used in path operations and in
# dependencies (synchrounous ones in a thread pool, async ones in the main
# thread's event loop); keep this in mind when writing your own functions.
# Should write some profiling code to check where performance can be improved!


settings = conf.get_config()
settings.show()

app = FastAPI()


# CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["Content-Range"],
)

# routes
for r in [auth_routes, adm_routes, user_routes, results_routes, ws_redis_routes]:
    app.include_router(r.router)


@app.on_event("startup")
def startup_event():
    # u.make_dir(settings.plots_dir)
    app.state.redis = redis_u.redis_client()
    print(f"created redis pool: {app.state.redis}")


@app.on_event("shutdown")
async def shutdown_event():
    await app.state.redis.connection_pool.disconnect()
    print("redis pool closed!")
