import functools as ft
from typing import List

from . import models as db_m

# these classes are used to pass around authenticated user information in non
# transactional contexts; they should be updated with a db query where necessary


class DBUser:
    _id: str
    access_token: str
    role: str
    active: bool

    def __init__(self, user_db: db_m.User):
        self._id = str(user_db)
        self.access_token = user_db.access_token
        self.role = user_db.type
        self.active = user_db.active

    def __repr__(self):
        return f"{self._id}"


class DBTeacher(DBUser):
    email: str

    def __init__(self, teacher_db: db_m.Teacher):
        super().__init__(teacher_db)
        self.email = teacher_db.email

    def __repr__(self):
        return super().__repr__()


# TODO check which fields are actually used
class DBStudent(DBUser):
    teacher_email: str
    code: str
    done_experiments: list
    doing_experiment: str
    next_experiment: str

    def __init__(self, student_db: db_m.Student):
        super().__init__(student_db)
        self.teacher_email = student_db.teacher.email
        self.code = student_db.code
        self.done_experiments = [exp.name for exp in student_db.done_experiments]
        self.doing_experiment = student_db.doing_experiment
        self.next_experiment = student_db.next_experiment

    def __repr__(self):
        return super().__repr__()


# STUDENTS REPRESENTATION STORED IN FRONTEND
# {
#     student_code_1: {
#         'username': <class 'str'>,
#         'active': <class 'bool'>,
#         'exp_info': {
#              'done': <class 'list'>,
#              'doing': <class 'str'>
#         }
#     },
#     student_code_2: {
#         ...
#     },
#  ...
# }
def student_repr(student: db_m.Student) -> dict:
    return {
        student.code: {
            "active": student.active,
            "username": student.username,
            "exp_info": {
                "done": [exp.name for exp in student.done_experiments],
                "doing": student.doing_experiment,
            },
        }
    }


def students_repr(
    students: List[db_m.Student],
    condition: callable = lambda *_, **__: True,
    *cond_args,
    **cond_kwargs,
) -> dict:
    return ft.reduce(
        lambda acc, stud_repr: {**acc, **stud_repr},
        [
            student_repr(stud)
            for stud in students
            if condition(stud, *cond_args, *cond_kwargs)
        ],
        {},
    )


def make_user_schema(user_db: db_m.User) -> DBUser:
    return creator.get(user_db.type, DBUser)(user_db)


creator = {"teachers": DBTeacher, "students": DBStudent}
