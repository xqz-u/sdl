import functools as ft
from typing import List, Union

from sqlalchemy.orm import Session

from . import models as db_m
from . import utils as db_u

# NOTE sqlAlchemy suppports async code! not taking advantage of it right now


def get_user(sess: Session, _id: int):
    return sess.query(db_m.User).filter_by(id=_id).first()


def get_all_users(
    session: Session,
) -> List[Union[db_m.Admin, db_m.Teacher, db_m.Student]]:
    return session.query(db_m.User).all()


def get_entities(
    session: Session, table: db_m.TableInfo
) -> List[Union[db_m.Teacher, db_m.Admin, db_m.Student, db_m.Experiment]]:
    return session.query(table).all()


def get_user_by_email(
    session: Session,
    user_email: str,
    user_model: Union[db_m.Admin, db_m.Teacher] = db_m.RegisteredUser,
) -> db_m.RegisteredUser:
    return session.query(user_model).filter_by(email=user_email).first()


get_teacher = ft.partial(get_user_by_email, user_model=db_m.Teacher)
get_admin = ft.partial(get_user_by_email, user_model=db_m.Admin)


def get_auth_user(session: Session, session_tok: str) -> db_m.User:
    return (
        session.query(db_m.User).filter_by(access_token=session_tok).first()
        if session_tok
        else None
    )


# NOTE see
# https://stackoverflow.com/questions/30598714/one-to-one-delete-for-sqlalchemy-classes-with-inheritance
# as why we delete users like this, and remember to commit after
# NOTE user experiments rusults are not deleted from join table!!
def delete_user(sess: Session, user_db: db_m.User) -> int:
    return sess.delete(user_db)


def delete_student_results(sess: Session, user: db_m.User):
    for res in sess.query(db_m.TestResult).filter_by(author_id=user.id).all():
        sess.delete(res)


# deletes all students and all their results, given a teacher
def delete_classroom(sess: Session, teacher: db_m.Teacher):
    for st in teacher.students:
        delete_student_results(sess, st)
        delete_user(sess, st)


def get_student(session: Session, code: str) -> db_m.Student:
    return session.query(db_m.Student).filter_by(code=code).first()


def get_experiment(session: Session, exp_name: str) -> db_m.Experiment:
    return session.query(db_m.Experiment).filter_by(name=exp_name).first()


def get_all_experiments(session: Session) -> List[db_m.Experiment]:
    return session.query(db_m.Experiment).all()


def get_results_by_id(session: Session, _id: int) -> List[db_m.TestResult]:
    return session.query(db_m.TestResult).filter_by(author_id=_id).all()


def create_account(
    session: Session,
    email: str,
    plain_password: str,
    account_model: db_m.RegisteredUser = db_m.Teacher,
) -> db_m.RegisteredUser:
    new_user = account_model(
        email=email, password_hash=db_u.hash_password(plain_password)
    )
    db_u.modify_session_entry(session, new_user)
    return new_user


def create_students(sess: Session, codes: list, teacher_db: db_m.Teacher) -> list:

    students = [db_m.Student(code=c, teacher=teacher_db) for c in codes]
    db_u.modify_session_entry(sess, teacher_db, *students)
    return students
