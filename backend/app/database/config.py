from passlib.context import CryptContext
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .. import config as conf


# allow passing a Settings instance from config for interactive development,
# sys.path is not properly set
def session_factory(settings: conf.Settings = None) -> sessionmaker:
    return sessionmaker(
        bind=create_engine(
            settings.x_db_url if settings else conf.get_config().x_db_url,
            connect_args={"check_same_thread": False},
        )
    )


# SessionLocal() returns an instance of sqlalchemy.orm.Session; comment next
# line for interactive development TODO better approach
SessionLocal = session_factory()

# encryption tool
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
