import contextlib as ctx
import time

from sqlalchemy.orm import Session

from . import config as db_cfg
from . import models as db_m


@ctx.contextmanager
def Transaction():
    session = db_cfg.SessionLocal()
    try:
        yield session
    finally:
        session.close()


# NOTE should re-raise to handle in route's body, but the exception is not
# caught there; needs fix. rn only HTTPException from fastapi are supported,
# see here
# https://fastapi.tiangolo.com/tutorial/dependencies/dependencies-with-yield/?h=depende#dependencies-with-yield-and-httpexception
def modify_session_entry(session: Session, *db_tables, error: BaseException = None):
    session.add_all(db_tables)
    try:
        session.commit()
    except Exception as e:
        print(e)
        print("rolling back...")
        session.rollback()
        if error:
            raise error


# used for user creation by admin
def hash_password(password: str) -> str:
    return db_cfg.pwd_context.hash(password)


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return db_cfg.pwd_context.verify(plain_password, hashed_password)


# generate unique `n_students` codes: teacher id + timestamp + student index;
# the latter is used to keep consistency in indices when student codes are
# added after a first generation
def create_student_codes(n: int, teacher_db: db_m.Teacher) -> list:
    timestamp = int(time.time())
    return [
        f"{teacher_db.id}-{timestamp}-{teacher_db.n_students + i}" for i in range(n)
    ]


# normalize those experiment field values given by jatos to valid sqlalchemy
# datatypes
def normalize_values(init_fields: dict) -> dict:
    return {
        k: v
        if not db_m.TestResult.exp_field_normalizer.get(k)
        else db_m.TestResult.exp_field_normalizer[k](v)
        for k, v in init_fields.items()
    }
