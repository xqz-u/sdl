from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    MetaData,
    String,
    Table,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import current_timestamp

from .. import config as conf
from .. import utils as u

settings = conf.get_config()

# from
# https://alembic.sqlalchemy.org/en/latest/naming.html#autogen-naming-conventions
# #to avoid
# #https://alembic.sqlalchemy.org/en/latest/batch.html?highlight=drop%20constraint,
# fuck sqlite
meta = MetaData(
    naming_convention={
        "ix": "ix_%(column_0_label)s",
        "uq": "uq_%(table_name)s_%(column_0_name)s",
        "ck": "ck_%(table_name)s_%(constraint_name)s",
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
        "pk": "pk_%(table_name)s",
    }
)

Base = declarative_base(metadata=meta)


class TableInfo(Base):
    __abstract__ = True

    date_created = Column(DateTime, default=current_timestamp())
    date_modified = Column(
        DateTime, default=current_timestamp(), onupdate=current_timestamp()
    )


class User(TableInfo):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    type = Column(String)
    access_token = Column(String, default="")

    @property
    def active(self):
        return self.access_token != ""

    __mapper_args__ = {
        "polymorphic_on": type,
        "polymorphic_identity": "users",
    }


class RegisteredUser(User):
    __tablename__ = "registered_users"
    __mapper_args__ = {
        "polymorphic_identity": "registered_users",
    }

    id = Column(Integer, ForeignKey("users.id"), primary_key=True, index=True)
    email = Column(String, nullable=False, unique=True, index=True)
    password_hash = Column(String, nullable=False)

    def __repr__(self):
        return f"<{self.email}"


class Admin(RegisteredUser):
    __tablename__ = "admins"
    __mapper_args__ = {
        "polymorphic_identity": "admins",
    }

    id = Column(
        Integer, ForeignKey("registered_users.id"), primary_key=True, index=True
    )

    def __repr__(self):
        return f"{super().__repr__()}: Admin #{self.id}>"


class Teacher(RegisteredUser):
    __tablename__ = "teachers"
    __mapper_args__ = {"polymorphic_identity": "teachers"}

    id = Column(
        Integer, ForeignKey("registered_users.id"), primary_key=True, index=True
    )
    students = relationship(
        "Student", backref="teacher", foreign_keys="Student.teacher_id"
    )

    @property
    def n_students(self):
        return len(self.students)

    def __repr__(self):
        return f"{super().__repr__()}: Teacher #{self.id}>"


# join table for m-t-m relationship student/experiment
students_experiments = Table(
    "students_experiments",
    Base.metadata,
    Column("student_id", ForeignKey("students.id"), primary_key=True),
    Column("experiment_id", ForeignKey("experiments.id"), primary_key=True),
)


# NOTE next experiment follows the order of experiment declaration in
# jatos_config.json; an additional attribute 'order' could be used to control the
# order of experiments
class Student(User):
    __tablename__ = "students"
    __mapper_args__ = {"polymorphic_identity": "students"}

    id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    code = Column(String, nullable=False, unique=True)
    username = Column(String, nullable=False, default="")
    teacher_id = Column(Integer, ForeignKey("teachers.id"))
    doing_experiment = Column(String, default="")
    done_experiments = relationship(
        "Experiment", secondary=students_experiments, backref="authors"
    )

    @property
    def next_experiment(self):
        experiments_names = settings.experiments_names
        n_done_experiments = len(self.done_experiments)
        # do not move cursor after last experiment, always return it once all
        # experiments are done
        return (
            experiments_names[n_done_experiments]
            if n_done_experiments < len(experiments_names)
            else "END"
        )

    def __repr__(self):
        return f"<Student #{self.id} Code: {self.code} Teacher: {self.teacher_id}>"


class Experiment(TableInfo):
    __tablename__ = "experiments"

    id = Column(Integer, primary_key=True, index=True)
    results = relationship(
        "TestResult", backref="experiment", foreign_keys="TestResult.experiment_id"
    )
    name = Column(String, unique=True)

    def __repr__(self):
        return f"<Experiment #{self.id} name: {self.name}>"


class TestResult(TableInfo):
    __tablename__ = "test-results"

    id = Column(Integer, primary_key=True, index=True)
    author_id = Column(Integer, ForeignKey("users.id"))
    experiment_id = Column(Integer, ForeignKey("experiments.id"))
    # all tests, nullable defualts to True so not all fields must be given
    practice = Column(Boolean)
    response_time_keyboard_resp = Column(Integer)
    # context exp
    # NOTE only when practice == True then context == "0"; practice data
    # are not used
    context = Column(String)
    exp_dur = Column(Integer)
    # context exp and risk exp
    response_time_reproduction = Column(Integer)
    # risk exp
    punish = Column(Boolean)
    total_points = Column(Integer)  # NOTE unsure if this field is used

    # used to map jatos experiments fields to database columns of the declared
    # type
    exp_field_normalizer = {
        "practice": u.boolean_normalize,
        "punish": u.boolean_normalize,
    }

    def to_dict(self) -> dict:
        return {
            k: v
            for k in [
                "practice",
                "response_time_keyboard_resp",
                "response_time_reproduction",
                "context",
                "exp_dur",
                "punish",
                "total_points",
            ]
            if (v := getattr(self, k)) is not None
        }
