import inspect as insp
from dataclasses import dataclass
from typing import Optional

import aioredis
from fastapi.websockets import WebSocket, WebSocketDisconnect

from ..database import crud
from ..database import schemas as db_sch
from ..database import utils as db_u
from . import utils as redis_u


# TODO rethink student/teacher redis connections s.t. teacher can send messages
# to selected students; broadcasting functionality is not used
# NOTE messages/events have a corresponding handling function with the same name;
# this information is passed by providing the ConnManager with a submodule where
# to look for handlers, this is done in the __post__init__
@dataclass
class ConnManager:
    ws: WebSocket
    user_sch: db_sch.DBUser
    redis: aioredis.Redis = None
    dispatch: dict = None
    streams: tuple = ()
    latest_ids: str = "$"
    ws_connected: bool = True

    def __post__init__(self, submodule):
        self.redis = self.ws.app.state.redis
        self.dispatch = dict(insp.getmembers(submodule, insp.iscoroutinefunction))
        # add default `handle_die` handler, same for students and teachers
        self.dispatch.update({"handle_die": handle_die})

    def redis_log(self, msg: str):
        redis_u.timed_message(f"REDIS:{self.user_sch}: {msg}")

    def ws_log(self, msg: str):
        redis_u.timed_message(f"WS:{self.user_sch}: {msg}")

    def can_handle(self, action: str, logger: callable) -> Optional[callable]:
        maybe_handler = self.dispatch.get(action)
        if not maybe_handler:
            logger(f"No handler defined for `{action}`! continue")
        return maybe_handler

    async def initial_sync(self, old_events: list):
        self.redis_log("default initial_sync")

    async def ws_close_hook(self):
        self.redis_log("default ws_close_hook")

    async def on_ws_disconnect(self):
        self.ws_log("entered on disconnect...")
        self.ws_connected = False
        await self.ws_close_hook()  # perform possible actions before teardown

        # self.redis_log(
        #     f"free clients: {self.redis.connection_pool._available_connections}"
        # )

        # notify the reading loop to shut down (needs a new redis
        # client since the current one is blocked reading)
        await redis_u.client_from_pool(self.redis).xadd(
            self.streams[0],
            {"action": "handle_die", "author": self.user_sch.access_token},
        )
        self.ws_log(f"client disconnected, latest_ids: {self.latest_ids}")

    async def ws_handler(self):
        while self.ws_connected:
            self.ws_log("waiting to read...")
            try:
                data = await self.ws.receive_json()
            except WebSocketDisconnect:
                await self.on_ws_disconnect()
            else:
                action = data["action"]
                handler = self.can_handle(action, self.ws_log)
                if handler:
                    self.ws_log(f"START dispatching msg: `{action}`...")
                    await handler(self, data)
                    self.ws_log(f"DONE dispatching msg: `{action}`!")

    async def redis_handler(self):
        self.redis_log(f"check old events in `{self.streams[0]}`...")
        await self.initial_sync(await self.redis.xrevrange(self.streams[0]))
        while self.redis and self.ws_connected:
            self.redis_log("waiting for events...")
            events = await self.redis.xread({self.streams[0]: self.latest_ids}, block=0)
            # self.redis_log(f"I am {self.user_sch.access_token}")
            for [key, [[e_id, e]]] in events:
                # self.redis_log(f"{e_id} -> {e}")
                # dispatch events
                action = e["action"]
                handler = self.can_handle(action, self.redis_log)
                if not handler:
                    continue
                self.redis_log(f"START dispatching action: `{action}`...")
                next_event_id = await handler(
                    self, {"key": key, "id": e_id, "fields": e}
                )
                self.redis_log(f"DONE dispatching action: `{action}`...")
                # result of handling `handle_die` message
                if next_event_id == "die":
                    self.redis_log(f"received die message! {e_id}")
                    assert (
                        not self.ws_connected
                    ), f"Won't break from redis loop if ws_connected is not set to False! {self.user_sch}"
                    break
                # update `latest_ids` depending whether the dispatched function
                # did or didn't
                self.latest_ids = next_event_id or e_id
        self.redis_log("========= redis dead ==========")


# TODO try to hard close the redis connection, raise an exception to signal
# death and catch it in redis_handler to signal user disconnection. The current
# method is too convoluted, only the recipient of the disconnection should know
# about it and not the whole classroom.
# Or, create another stream for miscellaneous messages, but this is more
# asyncio intensive
# redis
async def handle_die(mng: ConnManager, event_info: dict) -> Optional[str]:
    event = event_info["fields"]
    author = event["author"]
    # every other peer receives the shutdown message a peer sends to itself to
    # notify shutdown, so each peer must make sure it is the right recipient of
    # the message
    if author == mng.user_sch.access_token:
        if mng.user_sch.role == "students":
            with db_u.Transaction() as sess:
                stud_db = crud.get_student(sess, mng.user_sch.code)
                stud_db.doing_experiment = ""
                db_u.modify_session_entry(sess, stud_db)
        return "die"
