from datetime import datetime

import aioredis

from .. import config as conf
from ..database import schemas as db_sch

settings = conf.get_config()


def timed_message(msg: str):
    print(f"{datetime.now().strftime('%H:%M:%S:%f')}: {msg}")


def redis_client(config: tuple = settings.redis_conf) -> aioredis.Redis:
    """
    Creates a new Redis client backed by a connection pool, accessible via
    `client.connection_pool`
    """
    address, opts = config
    try:
        return aioredis.from_url(address, **opts)
    except ConnectionRefusedError:
        print(f"cannot open redis connection pool on: {address}")


def client_from_pool(redis: aioredis.Redis) -> aioredis.Redis:
    """
    Returns a new Redis client taken from an existing connection pool.
    The new connection is added to the
    `aioredis.ConnectionPool._available_connections` list of the passed
    instance, so that it can be reused and teared down once the whole pool
    disconnects.

    NOTE this utility should be used when a new connection is necessary, e.g. if
    the one currently used is in a blocking state (Redis is single-threaded); in
    all other cases it is best to rely on the (possibly new) connection returned
    by `aioredis.ConnectionPool.execute_command` function.
    See https://github.com/aio-libs/aioredis-py/issues/772
    """
    return aioredis.Redis(connection_pool=redis.connection_pool)


# tuple[0] = reading stream, tuple[1] = writing stream
def make_rw_streams(user_sch: db_sch.DBUser) -> tuple:
    if user_sch.role == "teachers":
        return f"{user_sch.email}:r", f"{user_sch.email}:w"
    assert user_sch.role == "students", f"Wrong user schema role: {user_sch.role}"
    return f"{user_sch.teacher_email}:w", f"{user_sch.teacher_email}:r"
