import sys
from dataclasses import dataclass

from ..database import crud
from ..database import schemas as db_sch
from ..database import utils as db_u
from ..ws_redis import utils as redis_u
from . import connection_manager as conn_man


@dataclass
class TeacherConnManager(conn_man.ConnManager):
    teacher_sch: db_sch.DBTeacher = None

    def __post_init__(self):
        super().__post__init__(sys.modules[__name__])
        self.teacher_sch = self.user_sch  # to simplify naming
        self.streams = redis_u.make_rw_streams(self.teacher_sch)


# ws
# payload: str
async def register_students(mng: TeacherConnManager, message: dict):
    n_students = int(message["payload"])
    # open student accounts and persist them to database
    with db_u.Transaction() as sess:
        teacher_db = crud.get_auth_user(sess, mng.teacher_sch.access_token)
        codes = db_u.create_student_codes(n_students, teacher_db)
        students_repr = db_sch.students_repr(
            crud.create_students(sess, codes, teacher_db)
        )
    # send codes and student status back to client for display
    await mng.ws.send_json({"action": "register_students", "payload": students_repr})


# redis
async def student_status(mng: TeacherConnManager, event_info: dict):
    event = event_info["fields"]
    # update teacher client with new students status
    author = event["author"]
    with db_u.Transaction() as sess:
        student_repr = db_sch.student_repr(crud.get_student(sess, author))
    await mng.ws.send_json({"action": event["action"], "payload": student_repr})
