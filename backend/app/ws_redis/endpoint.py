import asyncio

from fastapi import APIRouter, Depends
from fastapi.websockets import WebSocket

from .. import dependencies as deps
from ..database import schemas as db_sch
from ..jatos_experiments import controller as jatos_c
from . import student_connector as student_mng
from . import teacher_connector as teacher_mng

router = APIRouter(tags=["WebSocket"])

connection_managers = {
    "teachers": teacher_mng.TeacherConnManager,
    "students": student_mng.StudentConnManager,
}


count = 0


def incf(amount=1):
    global count
    count += amount
    print(f"total connections: {count}")


# TODO return websocket class exceptions to client instead of custom messages
@router.websocket("/ws")
async def endpoint(
    ws: WebSocket, user_sch: db_sch.DBUser = Depends(deps.get_user_schema)
):
    await ws.accept()
    incf()
    conn_mng = connection_managers[user_sch.role](ws, user_sch)
    await asyncio.gather(conn_mng.ws_handler(), conn_mng.redis_handler())
    print(f"===== Goodbye, {user_sch} =====")
    incf(-1)


# NOTE this handler could be integrated with ConnectionManager.ws_handler, skip
# now for simplicity...
@router.websocket("/ws/jatos")
async def jatos_endpoint(
    ws: WebSocket,
    exp_name: str,
    user_sch: db_sch.DBUser = Depends(deps.get_user_schema),
):
    print(f"NEW WS JATOS -> {user_sch} exp {exp_name}")
    await ws.accept()
    incf()
    await asyncio.gather(jatos_c.jatos_ws_handler(ws, user_sch, exp_name))
    print(f"===== Goodbye, JATOS {user_sch} =====")
    incf(-1)


# instead of asyncio.gather, don't remember why I changed
# bg_tasks = map(
#     lambda coro: asyncio.create_task(coro()),
#     [conn_mng.ws_handler, conn_mng.redis_handler],
# )
# done, pending = await asyncio.wait(bg_tasks)
# for task in pending:
#     print(f"pending task: {task}, cancel")
#     task.cancel()
