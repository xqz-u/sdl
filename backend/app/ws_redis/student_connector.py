import sys
from dataclasses import dataclass

# from .. import config
from ..database import crud
from ..database import schemas as db_sch
from ..database import utils as db_u
from ..ws_redis import utils as redis_u
from . import connection_manager as conn_man

# print("getting config in student redis connector...")
# conf = config.get_config()


@dataclass
class StudentConnManager(conn_man.ConnManager):
    student_sch: db_sch.DBStudent = None

    def __post_init__(self):
        super().__post__init__(sys.modules[__name__])
        self.student_sch = self.user_sch  # to simplify naming
        # inverted w.r.t. teacher's streams
        self.streams = redis_u.make_rw_streams(self.student_sch)

    async def student_update(self):
        self.latest_ids = await self.redis.xadd(
            self.streams[1],
            {
                "action": "student_status",
                "author": self.student_sch.code,
            },
        )

    async def initial_sync(self, _: list = []):
        # notify teacher of your status (in/offline), and start listening from
        # this event
        await self.student_update()

    async def ws_close_hook(self):
        # notify teacher client student is offline
        await self.student_update()


# ws
# payload: {doing: str}
# before starting an experiment on jatos, update user's model in backend
# TODO should return a confirmation to frontend to avoid inconsistent states...
async def experiment_status(mng: StudentConnManager, message: dict):
    doing_exp = message["payload"]["doing"]
    # update student experiment status in db and refresh student's schema
    with db_u.Transaction() as sess:
        stud_db = crud.get_student(sess, mng.student_sch.code)
        stud_db.doing_experiment = doing_exp
        db_u.modify_session_entry(sess, stud_db)
        mng.student_sch = db_sch.DBStudent(stud_db)
    # update teacher client saying that student is busy
    await mng.student_update()


# NOTE this should basically be the same were a teacher to perform the
# experiment, except for notifying its teacher
# redis
async def finished_experiment(mng: StudentConnManager, event_info: dict):
    event = event_info["fields"]
    author = event["author"]
    # check that message is intended for you (all classroom peers receive it)
    if not author == mng.student_sch.access_token:
        return
    # change doing experiment status, and add last experiment to student's
    # completed experiments
    with db_u.Transaction() as sess:
        stud_db = crud.get_student(sess, mng.student_sch.code)
        last_experiment = stud_db.doing_experiment
        mng.redis_log(
            f"done with experiment {stud_db.doing_experiment}, tell student client..."
        )
        exp_db = crud.get_experiment(sess, stud_db.doing_experiment)
        stud_db.doing_experiment = ""
        stud_db.done_experiments.append(exp_db)
        db_u.modify_session_entry(sess, stud_db, exp_db)
        mng.student_sch = db_sch.DBStudent(stud_db)
    # update student client recipient
    await mng.ws.send_json(
        {
            "action": "experiment_status",
            "payload": {"next_experiment": mng.student_sch.next_experiment},
        }
    )
    # finally, update student's teacher with student's status
    mng.redis_log("tell teacher client...")
    await mng.student_update()
    mng.redis_log(f"DONE EVENTS EXPERIMENT {last_experiment}!")
