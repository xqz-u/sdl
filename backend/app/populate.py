from . import config as conf
from .database import crud
from .database import models as db_m
from .database import utils as db_u


# used to create teacher and admin tables
def create_registered_users(specs):
    with db_u.Transaction() as sess:
        for el in specs:
            crud.create_account(sess, *el)


def create_experiments(names):
    with db_u.Transaction() as sess:
        exps = [db_m.Experiment(name=n) for n in names]
        db_u.modify_session_entry(sess, *exps)


reg_users = [
    ["rt@gmail.com", "real_teacher", db_m.Teacher],
    ["rt2@gmail.com", "rt2", db_m.Teacher],
    ["admin@gmail.com", "admin", db_m.Admin],
]

settings = conf.get_config()
settings.show()

create_registered_users(reg_users)
create_experiments(settings.experiments_names)
