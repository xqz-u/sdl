# NOTE these function should be used offline to produce fake data
import random
import uuid
from typing import List

from backend.app import utils as u
from backend.app.database import crud
from backend.app.database import models as db_m
from backend.app.database import utils as db_u
from sqlalchemy.orm import Session


def wrap_test_data(
    test_id: int, student_id: int, student_data: List[dict]
) -> List[db_m.TestResult]:
    """Wrap a list of data in a TestResult instance, return a list of TestResult"""
    return [
        db_m.TestResult(
            experiment_id=test_id, author_id=student_id, **db_u.normalize_values(d)
        )
        for d in student_data
    ]


def mock_tur(
    exp_id: int, student_id: int, reps: int, len_punish_context: int
) -> List[db_m.TestResult]:
    assert len_punish_context <= reps
    print(f"mock {reps} risk for stud {student_id}")
    return wrap_test_data(
        exp_id,
        student_id,
        [
            {
                "practice": "no",
                "response_time_keyboard_resp": random.randint(200, 8000),
                "response_time_reproduction": random.randint(200, 8000),
                "punish": 0 if i <= len_punish_context else 1,
                "total_points": random.randint(400, 1600),
            }
            for i in range(reps)
        ],
    )


def mock_context(
    exp_id: int, student_id: int, reps: int, len_900_context: int
) -> List[db_m.TestResult]:
    assert len_900_context <= reps
    print(f"mock {reps} context for stud {student_id}")
    return wrap_test_data(
        exp_id,
        student_id,
        [
            {
                "practice": "no",
                "response_time_keyboard_resp": random.randint(200, 8000),
                "response_time_reproduction": random.randint(200, 8000),
                "context": random.choice(["long", "short"]),
                "exp_dur": v,
            }
            for v in [900 for _ in range(len_900_context)]
            + [random.choice([500, 1300, 1500]) for _ in range(reps - len_900_context)]
        ],
    )


def mock_1sec(exp_id: int, student_id: int, reps: int) -> List[db_m.TestResult]:
    print(f"mock {reps} 1_sec for stud {student_id}")
    return wrap_test_data(
        exp_id,
        student_id,
        [
            {
                "practice": "no",
                "response_time_keyboard_resp": random.randint(200, 8000),
            }
            for _ in range(reps)
        ],
    )


known_exp_mock_fns = {"1_second": mock_1sec, "context": mock_context, "tur": mock_tur}


# NOTE 'args' stores the desired repetitions number in its first position
def mock_students_data(
    sess: Session, mock_students: List[db_m.Student], exp: str, args: list
) -> List[List[db_m.TestResult]]:
    if not (exp_mock_fn := known_exp_mock_fns.get(exp)):
        print(f"Do not know how to mock exp {exp}, skip")
        return []
    exp = sess.query(db_m.Experiment).filter_by(name=exp).first()
    ret = [exp_mock_fn(exp.id, stud.id, *args) for stud in mock_students]
    for st in mock_students:
        st.done_experiments.append(exp)
    print(f"DONE mocking exp {exp}, {exp.id}")
    return ret


# NOTE side effectful on database
def mock_class_data(
    sess: Session, teacher: db_m.Teacher, n_students: int, **exp_and_args: dict
) -> List[db_m.TestResult]:
    # check that we know how to mock at least one experiment before mocking
    # students
    if not any([known_exp_mock_fns.get(exp) for exp in exp_and_args]):
        print("No way to mock any experiment! Abort")
        return
    # mock and persist students
    mock_students = [
        db_m.Student(code=str(uuid.uuid4()), teacher_id=teacher.id)
        for _ in range(n_students)
    ]
    db_u.modify_session_entry(sess, *mock_students)
    # mock some results for each student, for each experiment given and able to
    # handle
    all_res = u.flatten(
        [
            mock_students_data(sess, mock_students, exp, args)
            for exp, args in exp_and_args.items()
        ]
    )
    print(f"created {len(all_res)} TestResult instances")
    db_u.modify_session_entry(sess, *all_res, *mock_students)
    return all_res


def mock_student_data(stud_code: str, **mock_spec: dict):
    with db_u.Transaction() as sess:
        stud_db = crud.get_student(sess, stud_code)
        all_results = [
            mock_students_data(sess, [stud_db], exp, args)
            for exp, args in mock_spec.items()
        ]
        print(f"created {len(all_results)} TestResult instances")
        db_u.modify_session_entry(sess, *u.flatten(all_results), stud_db)


# mock_student_data("2-1631284169-1", **{"context": [50, 25]})

# mock_student_data("2-1631284169-1", **{"1_second": [50], "tur": [50, 25]})
