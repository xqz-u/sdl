from functools import partial

from ..database import models as db_m


def check_db_model(model, table_instance):
    return isinstance(table_instance, model)


is_admin = partial(check_db_model, db_m.Admin)
is_teacher = partial(check_db_model, db_m.Teacher)
is_student = partial(check_db_model, db_m.Student)
