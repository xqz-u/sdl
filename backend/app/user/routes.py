from fastapi import APIRouter, Depends

from .. import config as conf
from .. import dependencies as deps
from ..auth import utils as auth_u
from ..database import schemas as db_sch
from . import schemas as user_sch
from . import utils as user_u

router = APIRouter(prefix="/user", tags=["User"])

settings = conf.get_config()


# NOTE the sqlalchemy session is not used in these routes, although passed by
# the dependencies; this ensures the object loaded from the db (user etc) are
# not expired, since the session stays open for the whole request


@router.get("/teacher-init-sync", response_model=user_sch.ActionResponse)
async def teacher_init_sync(
    user_sess: tuple = Depends(deps.get_active_user_and_session),
):
    teacher_db, sess = user_sess
    if not user_u.is_teacher(teacher_db):
        print(f"user is not teacher: {teacher_db}")
        return auth_u.permission_error()
    return user_sch.ActionResponse(
        payload={
            "experiments": settings.jatos_experiments_conf,
            "students": db_sch.students_repr(teacher_db.students),
        },
    )


@router.get("/student-init-sync", response_model=user_sch.ActionResponse)
async def student_init_sync(
    user_sess: tuple = Depends(deps.get_active_user_and_session),
):
    student_db, sess = user_sess
    if not user_u.is_student(student_db):
        print(f"user is not student: {student_db}")
        return auth_u.permission_error()
    return user_sch.ActionResponse(
        payload={
            "experiments": settings.jatos_experiments_conf,
            "next_experiment": student_db.next_experiment,
        },
    )
