from typing import Optional

from fastapi import APIRouter, Depends

from ... import dependencies as deps
from ...auth import utils as auth_u
from ...database import crud
from ...jatos_experiments import controller as jatos_c
from .. import schemas as user_sch
from .. import utils as user_u

router = APIRouter(prefix="/user/results", tags=["Results"])


# NOTE much of the ifs would disappear if a db model higher in the hierarchy
# than student were able to do experiments

# this route is used by a student asking for his own plots data, or by a
# teacher/admin to get the plots data of a particular student indicating the
# student code as query parameter
@router.get("/student", response_model=user_sch.ActionResponse)
async def student_results(
    code: Optional[str] = None,
    user_sess: tuple = Depends(deps.get_active_user_and_session),
):
    user_db, sess = user_sess
    student_db = user_db
    # student agent who requested its own data
    if user_u.is_student(student_db):
        if code and student_db.code != code:
            # TODO correct error
            return auth_u.permission_error()
    else:
        # tacher agent who asked for the results of a student
        if not code:
            # TODO correct error
            return auth_u.permission_error()
        student_db = crud.get_student(sess, code)
    return user_sch.ActionResponse(
        payload={
            "student": student_db.code,
            "plots_data": jatos_c.single_stud_plot_data(sess, student_db),
        }
    )
