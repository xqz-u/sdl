from pydantic import BaseModel


class ActionResponse(BaseModel):
    payload: dict
