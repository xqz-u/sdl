import functools as ft
import json
import os
from pathlib import Path
from pprint import pp

import dotenv as env
from pydantic import BaseSettings

# TODO logging
current_dir = Path(os.path.dirname(__file__))
basedir = current_dir.parent.absolute()


# variables precedence:
# - .env
# - defaults

class Settings(BaseSettings):
    # "x" will correspond to one of the ENV variable values; this field is
    # unused and left here for reference
    _x_db_url: str = ""
    jatos_experiments_conf: dict = {}
    aioredis_url: str = "redis://localhost?db=0"
    origins: list = []
    runtime_config: dict = {
        "host": "0.0.0.0",
        "port": 8000,
        "loop": "uvloop",
    }

    class Config:
        case_sensitive = False

    @property
    def x_db_url(self):
        return self._x_db_url

    @property
    def redis_conf(self):
        return (self.aioredis_url, {"encoding": "utf-8", "decode_responses": True})

    @property
    def experiments_names(self):
        return list(self.jatos_experiments_conf.keys())

    def show(self):
        pp(self.dict())


class DevSettings(Settings):
    dev_db_url: str = f"sqlite:///{basedir}/sdl-dev.db"
    origins: list = ["http://localhost:3000"]
    runtime_config: dict = {
        "host": "0.0.0.0",
        "port": 8000,
        "loop": "uvloop",
        "reload": True,
        "reload_dirs": ["app"],
    }

    @property
    def x_db_url(self):
        return self.dev_db_url


class ProdSettings (Settings):
    prod_db_url: str = f"sqlite:///{basedir}/sdl-prod.db"
    origins: list = ["*"]

    @property
    def x_db_url(self):
        return self.prod_db_url


class TestSettings(Settings):
    test_db_url: str = f"sqlite:///{basedir}/sdl-test.db"
    origins: list = ["http://localhost:3000"]

    @property
    def x_db_url(self):
        return self.test_db_url


# ... add testing and production config classes
configs = {"dev": DevSettings, "test": TestSettings, "prod": ProdSettings}


def create_config() -> Settings:
    env_file = env.find_dotenv()
    env.load_dotenv(env_file)
    config_class = configs[os.environ.get("ENV", "dev")]
    # allow pydantic to deserialize automatcally config fields if given by .env
    conf = config_class(_env_file=env_file) if env_file else config_class()
    # read the jatos experiment configuration at startup to avoid reading from disk each time the
    # corresponding route is called; we lose live reloading of the config file for this reason
    jatos_config_path = os.environ.get(
        "jatos_config_file", os.path.join(basedir, "jatos_config.json")
    )
    with open(jatos_config_path, "r") as fp:
        conf.jatos_experiments_conf = json.load(fp)
    return conf


@ft.lru_cache
def get_config() -> Settings:
    return create_config()
