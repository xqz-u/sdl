"""total points as int

Revision ID: 92a6ac91cd5d
Revises: 1bd8adeb190d
Create Date: 2021-08-09 02:43:47.953585

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "92a6ac91cd5d"
down_revision = "1bd8adeb190d"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # NOTE nope generated by marco...
    with op.batch_alter_table("test-results", schema=None) as batch_op:
        batch_op.drop_column("total_points")
        batch_op.add_column(sa.Column("total_points", sa.Integer(), nullable=True))
        batch_op.drop_column("punish")
        batch_op.add_column(sa.Column("punish", sa.Boolean(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
