import logging
import os
from logging.config import fileConfig

from alembic import context
from sqlalchemy import create_engine, engine_from_config, pool

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)
logger = logging.getLogger("alembic")

# add your model's MetaData object here
# for 'autogenerate' support
from app.database import models as db_m

target_metadata = db_m.Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.

# load env to get db url corresponding to env type (prod, dev) and developer's
# path
import dotenv as env

env_file = env.find_dotenv()
if env_file:
    logger.info(f"loding env from {env_file}...")
    env.load_dotenv(dotenv_path=env_file)


# similar functionality can be achieved with context.get_x_argument
def get_database_spec(migration_type: str = "online"):
    env = os.environ.get("ENV", "dev")
    db_url = os.environ.get(f"{env.upper()}_DB_URL")
    if migration_type == "online":
        return (
            db_url,
            (
                create_engine(db_url)
                if db_url
                else engine_from_config(
                    config.get_section(config.config_ini_section),
                    prefix="sqlalchemy.",
                    poolclass=pool.NullPool,
                )
            ),
        )
    else:
        return db_url or config.get_main_option("sqlalchemy.url")


# NOTE workflow:
# work with database in dev environment
# apply changes, then migrate the dev database
# when deploying, migrate the prod database
# passes:
# alembic revision --autogenerate -m message
# alembic upgrade head

# generates migrations as SQL script, runs with the --sql flag
def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = get_database_spec(migration_type="offline")
    logger.info(f"run offline migration, db: {url}")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    url, connectable = get_database_spec()
    logger.info(f"run online migration, db: {url}")

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata, render_as_batch=True
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
