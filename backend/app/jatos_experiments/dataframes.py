from typing import Dict, List

import pandas as pd
from sqlalchemy.orm import Session

from ..database import models as db_m


def student_results(
    sess: Session, student: db_m.Student
) -> Dict[str, List[db_m.TestResult]]:
    return {
        exp.name: sess.query(db_m.TestResult)
        .filter_by(experiment_id=exp.id, author_id=student.id)
        .all()
        for exp in sess.query(db_m.Experiment).all()
    }


def class_results(
    sess: Session, teacher: db_m.Teacher
) -> Dict[str, List[db_m.TestResult]]:
    return {
        exp.name: sess.query(db_m.TestResult)
        .filter_by(experiment_id=exp.id)
        .join(db_m.Student)
        .filter_by(teacher_id=teacher.id)
        .all()
        for exp in sess.query(db_m.Experiment).all()
    }


def national_results(sess: Session) -> Dict[str, List[db_m.TestResult]]:
    return {
        exp.name: sess.query(db_m.TestResult).filter_by(experiment_id=exp.id).all()
        for exp in sess.query(db_m.Experiment).all()
    }


# {exp0: {col0: [...], col1: [...]}, exp1: {...}}


# TODO uso sql views for optimized querys and data manipulation instead of
# pandas
def to_pandas(db_data: Dict[str, db_m.TestResult]) -> Dict[str, pd.DataFrame]:
    return {
        exp_name: pd.DataFrame(map(lambda res: res.to_dict(), exp_data))
        for exp_name, exp_data in db_data.items()
    }


# create a single dataframe with results for every experiment grouped by
# section (class, national, individual)
def group_results(
    stud_res: Dict[str, pd.DataFrame],
    class_res: Dict[str, pd.DataFrame],
    nation_res: Dict[str, pd.DataFrame],
) -> Dict[str, pd.DataFrame]:
    ret = {}
    # add 'group' column to all experiments of all dataframes
    for exp_name in stud_res.keys():
        for group_res, group_name in zip(
            [stud_res, class_res, nation_res], ["Leerling", "Klas", "Landelijk"]
        ):
            group_res[exp_name]["group"] = group_name  # modifies in-place
        # merge experiment dataframes in a single one that contains group
        # information
        ret[exp_name] = pd.concat(
            [ddf[exp_name] for ddf in [stud_res, class_res, nation_res]]
        )
    return ret


def base_preproc(df: pd.DataFrame) -> pd.DataFrame:
    return df[~(df["practice"]) & (df["response_time_keyboard_resp"] <= 5000)]


def preproc_context(df: pd.DataFrame) -> pd.DataFrame:
    df = base_preproc(df)
    return df[df["exp_dur"] == 900]


all_filters = {
    "1_second": base_preproc,
    "context": preproc_context,
    "tur": base_preproc,
}


# NOTE apply all preprocessing/filterations here and drop unused 'practice'
# column
def preproc_all_results(df_dict: Dict[str, db_m.TestResult]) -> Dict[str, pd.DataFrame]:
    return {
        exp_name: preproc_fn(df).drop("practice", axis=1)
        if not df.empty and (preproc_fn := all_filters.get(exp_name))
        else df
        for exp_name, df in to_pandas(df_dict).items()
    }


# the following function should be run on the dataframe of an experiment, with
# levels for the different groups added, to get data plottable on the frontend
# by plotly.js

# NOTE using pandas' not ~ the filteration fails?!
def sendable_tur(df: pd.DataFrame) -> List[Dict[str, pd.Series]]:
    return [
        {
            "x": df["group"][df["punish"]],
            "y": df["response_time_reproduction"][df["punish"]],
        },
        {
            "x": df["group"][df["punish"] == False],
            "y": df["response_time_reproduction"][df["punish"] == False],
        },
    ]


def sendable_context(df: pd.DataFrame) -> List[Dict[str, pd.Series]]:
    return [
        {
            "x": df["group"][df["context"] == "long"],
            "y": df["response_time_reproduction"][df["context"] == "long"],
        },
        {
            "x": df["group"][df["context"] == "short"],
            "y": df["response_time_reproduction"][df["context"] == "short"],
        },
    ]


def sendable_1second(df: pd.DataFrame) -> List[Dict[str, pd.Series]]:
    return [{"x": df["group"], "y": df["response_time_keyboard_resp"]}]


def cols_as_list(data: List[Dict[str, pd.Series]]) -> List[Dict[str, list]]:
    return [{k: v.to_list() for k, v in d.items()} for d in data]


# TODO make wire_fns parameter
def sendable_experiments_data(
    data: Dict[str, pd.DataFrame]
) -> Dict[str, List[Dict[str, list]]]:
    wire_fns = {
        "1_second": sendable_1second,
        "context": sendable_context,
        "tur": sendable_tur,
    }
    return {k: cols_as_list(wire_fns[k](v)) for k, v in data.items()}
