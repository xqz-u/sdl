from pprint import pp
from typing import Dict, List, Tuple, Union

from fastapi.websockets import WebSocket, WebSocketDisconnect
from sqlalchemy.orm import Session

from ..database import crud
from ..database import models as db_m
from ..database import schemas as db_sch
from ..database import utils as db_u
from ..ws_redis import utils as redis_u
from . import dataframes as df


async def jatos_ws_handler(ws: WebSocket, user_sch: db_sch.DBUser, exp_name: str):
    redis_pool = ws.app.state.redis
    redis_r_stream, _ = redis_u.make_rw_streams(user_sch)
    ws_connected = True
    with db_u.Transaction() as sess:
        experiment_id = crud.get_experiment(sess, exp_name).id
    print(f"JATOS starting experiment with id {experiment_id}")
    while ws_connected:
        try:
            data = await ws.receive_json()
        except WebSocketDisconnect:
            # jatos client disconnected: put this info on redis stream, and
            # handle it so that the concerned user clients will receive the
            # experiment status update via ws (see connection_manager)
            if isinstance(user_sch, db_sch.DBStudent):
                await redis_u.client_from_pool(redis_pool).xadd(
                    redis_r_stream,
                    {"action": "finished_experiment", "author": user_sch.access_token},
                )
            ws_connected = False
        else:
            print(f"JATOS {user_sch}")
            norm_data = db_u.normalize_values(data)
            pp(norm_data)
            # save test result data in student's database entry
            with db_u.Transaction() as sess:
                stud_db = crud.get_auth_user(sess, user_sch.access_token)
                res = db_m.TestResult(
                    **norm_data, author_id=stud_db.id, experiment_id=experiment_id
                )
                db_u.modify_session_entry(sess, res)
            print(f"JATOS added result to {user_sch}!")


def single_stud_plot_data(
    sess: Session, student_db: db_m.Student
) -> Dict[str, List[Dict[str, list]]]:
    nat_data = df.preproc_all_results(df.national_results(sess))
    class_data = df.preproc_all_results(df.class_results(sess, student_db.teacher))
    stud_data = df.preproc_all_results(df.student_results(sess, student_db))
    grouped_data = df.group_results(stud_data, class_data, nat_data)
    return df.sendable_experiments_data(grouped_data)


# wire_data structure:
# {
#     st_code_0: {
#         exp_0: [
#             {
#                 x: [...],
#                 y: [...]
#             },
#             {
#                 x: [...],
#                 y: [...]
#             },
#             ...
#         ],
#         exp_1: [
#             ...
#         ],
#         ...
#     },
#     st_code_1: {
#         ...
#     },
#     ...
# }
# TODO use for dowload all feature?
def class_plots_data(
    teacher_db: db_m.Teacher, sess: Session
) -> Tuple[
    List[Union[db_m.Teacher, db_m.Student]], Dict[str, Dict[str, List[Dict[str, list]]]]
]:
    # national data
    nat_data = df.preproc_all_results(df.national_results(sess))
    # class data
    class_data = df.preproc_all_results(df.class_results(sess, teacher_db))
    # for all students, generate data passed to frontend for plotting
    wire_data = {}
    mod_students = []
    for st in teacher_db.students:
        stud_data = df.preproc_all_results(df.student_results(sess, st))
        grouped_data = df.group_results(stud_data, class_data, nat_data)
        wire_data[st.code] = df.sendable_experiments_data(grouped_data)
        mod_students.append(st)
    return [teacher_db, *mod_students], wire_data
