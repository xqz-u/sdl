from typing import List, Union

from pydantic import BaseModel

from ..database import models as db_m


class DBExperiment(BaseModel):
    id: int
    name: str

    class Config:
        orm_mode = True


class DBUser(BaseModel):
    id: int

    class Config:
        orm_mode = True


class DBRegisteredUser(DBUser):
    email: str

    class Config:
        orm_mode = True


class DBTeacher(DBRegisteredUser):
    n_students: int

    class Config:
        orm_mode = True


class DBStudent(DBUser):
    code: str
    username: str
    teacher_id: int
    doing_experiment: str
    done_experiments: List[DBExperiment]

    class Config:
        orm_mode = True


models_converter = {
    db_m.Teacher: DBTeacher,
    db_m.Admin: DBRegisteredUser,
    db_m.Student: DBStudent,
    db_m.Experiment: DBExperiment,
}


def convert_user_models(
    entities: List[Union[db_m.Teacher, db_m.Admin, db_m.Student]]
) -> List[dict]:
    return [models_converter[type(ent)].from_orm(ent).dict() for ent in entities]


class TeacherCreate(BaseModel):
    email: str
    password: str
