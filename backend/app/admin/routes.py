from typing import List, Optional

from fastapi import APIRouter, Depends, Request, Response
from fastapi.exceptions import HTTPException

from .. import dependencies as deps
from ..database import crud
from ..database import models as db_m
from ..database import utils as db_u
from . import schemas as adm_sch

router = APIRouter(prefix="/admin", tags=["Admin"])


# NOTE always declaring react-admin query string parameters explicitly for
# swagger documentation
@router.get("/students", response_model=List[dict])
async def students(
    req: Request,
    response: Response,
    admin_db_sess: tuple = Depends(deps.get_admin_and_session),
    range: Optional[str] = None,
    filter: Optional[str] = None,
    sort: Optional[str] = None,
):
    _, sess = admin_db_sess
    students_db = crud.get_entities(sess, db_m.Student)
    response.headers["Content-Range"] = f"users */{len(students_db)}"
    return adm_sch.convert_user_models(students_db)


@router.get("/teachers", response_model=List[dict])
async def teachers(
    response: Response,
    admin_db_sess: tuple = Depends(deps.get_admin_and_session),
    range: Optional[str] = None,
    filter: Optional[str] = None,
    sort: Optional[str] = None,
):
    _, sess = admin_db_sess
    teachers_db = crud.get_entities(sess, db_m.Teacher)
    response.headers["Content-Range"] = f"users */{len(teachers_db)}"
    return adm_sch.convert_user_models(teachers_db)


# TODO retry strategy in case of race conditions (two ppl sign in with the same
# available username at the same time)
@router.post("/teachers")
async def create_teacher(
    teacher_spec: adm_sch.TeacherCreate,
    admin_db_sess: tuple = Depends(deps.get_admin_and_session),
):
    _, sess = admin_db_sess
    if crud.get_teacher(sess, teacher_spec.email):
        raise HTTPException(status_code=512, detail="Email already in use")
    new_teacher_db = crud.create_account(
        sess, teacher_spec.email, teacher_spec.password
    )
    db_u.modify_session_entry(
        sess, new_teacher_db, error=HTTPException(status_code=501)
    )
    return 200


@router.delete("/teachers/{teacher_id}")
async def delete_teacher(
    teacher_id: int,
    admin_db_sess: tuple = Depends(deps.get_admin_and_session),
):
    _, sess = admin_db_sess
    teacher_db = crud.get_user(sess, teacher_id)
    if not teacher_db:
        return HTTPException(status_code=404, detail=f"Don't know teacher {teacher_id}")
    crud.delete_user(sess, teacher_db)
    sess.commit()
    return 200
