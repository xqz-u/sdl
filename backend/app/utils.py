import asyncio
import json
from pathlib import Path


def mget(d: dict, *keys, raise_err: bool = True) -> list:
    ret = []
    for k in keys:
        v = None
        try:
            v = d[k]
        except KeyError:
            if raise_err:
                raise
        finally:
            ret.append(v)
    return ret


def boolean_normalize(v: str) -> bool:
    return v in ["yes", 1]


def flatten(seq: list) -> list:
    def inner(seq: list, acc: list):
        if seq == []:
            return acc
        car, cdr = seq[0], seq[1:]
        return (inner(car, []) if isinstance(car, list) else [car]) + inner(cdr, [])

    return inner(seq, [])


def parse_ra_qs(*els: list) -> list:
    return [json.loads(el) if el else el for el in els]


def make_dir(path: str):
    Path(path).mkdir(parents=True, exist_ok=True)


async def countdown(n):
    for i in reversed(range(0, n)):
        await asyncio.sleep(1)
        print(f"{i}", end="\r")
