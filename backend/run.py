#! /usr/bin/env python
if __name__ == "__main__":
    import uvicorn

    from app import config as conf

    uvicorn.run("app.main:app", **conf.get_config().runtime_config)

# ssl_certfile=f"{home}/cert.pem",
# ssl_keyfile=f"{home}/key.pem",
