import time

from app.database import utils as db_u, models as db_m


async def atest_perf(n):
    sess = next(db_u.database_session())
    print("create accounts...")
    time_ = int(time.time())
    accounts = [
        db_m.Teacher(email=f"{time_}_{i}_@gmail.com", password_hash="ciccio")
        for i in range(n)
    ]
    print("adding async...")
    db_u.modify_session_entry(sess, *accounts)


def test_perf(n):
    sess = next(db_u.database_session())
    print("create accounts...")
    time_ = int(time.time())
    accounts = [
        db_m.Teacher(email=f"{time_}_{i}_@gmail.com", password_hash="ciccio")
        for i in range(n)
    ]
    db_u.modify_session_entry(sess, *accounts)


def test():
    with db_u.Transaction() as sess:
        t = sess.query(db_m.Teacher).filter_by(email="rt2@gmail.com").first()
        s = db_m.Student(code="ciccio", teacher=t)
        t.students.append(s)
        db_u.modify_session_entry(sess, t, s)
        # print(t)
        # print(t.students)
    print("outside session scope...")
    time.sleep(5)
    print(t)
    print(t.students)


if __name__ == "__main__":
    test()
    # now = time.time()
    # asyncio.run(atest_perf(1000))
    # print(f"async exec time: {time.time() - now}")
    # now = time.time()
    # test_perf(1000)
    # print(f"sync exec time: {time.time() - now}")
