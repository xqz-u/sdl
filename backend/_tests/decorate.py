import functools as ft


def cache_first_call_arg(fn):
    creation_env_type = None

    def inner(env_type=None):
        nonlocal creation_env_type
        if not creation_env_type:
            creation_env_type = env_type
        print(f"im calledddd {env_type}")
        return fn(creation_env_type)

    return inner


@ft.lru_cache
@cache_first_call_arg
def add3(num=None):
    print(num)
    return num + 3
