import asyncio

from aioredis.errors import ConnectionClosedError as ServerConnectionClosedError

from . import redis_common_utils as redis_u


async def teacher_progression(
    streams: tuple, whoami: int, expected_students: int, read_count: int = 5
):
    def log(msg: str):
        redis_u.timed_message(f"teacher {whoami}: {msg}")

    pool = await redis_u.create_redis_pool()
    latest_ids = "$"
    rstream, wstream = streams
    synced_students = {}
    shut_sent = False
    first_run = True

    # students could have joined the 'room' before teacher (e.g. teacher
    # disconnection error); in this case, sync the teacher's status
    log("checking for lost events...")
    missed_events = await redis_u.sync_redis_client(pool, rstream)
    for event_id, event in missed_events:
        author = int(event["emitter"])
        event_code = event["payload"]
        if event_code == "student_connected":
            log(f"student {author} already connected!")
            synced_students[author] = True
        latest_ids = event_id

    log(f"start interactions! already in: {synced_students}")
    while pool:
        try:
            # once all peers have left, break
            if not (synced_students or first_run):
                break
            # once all peers have joined, tell them to leave, and start
            # listening for events newer than this one
            elif not shut_sent and len(synced_students) == expected_students:
                latest_ids = await pool.xadd(
                    wstream, {"payload": "STOP", "emitter": whoami}
                )
                shut_sent = True
                log(f".....sent termination message to all students: {latest_ids}")
            log(f"before teacher read from event: {latest_ids}...")
            events = await pool.xread(
                [rstream], count=read_count, latest_ids=[latest_ids]
            )
            for _, event_id, event in events:
                latest_ids = event_id
                author = int(event["emitter"])
                # dispatch events
                event_code = event["payload"]
                if event_code == "student_connected":
                    # if teacher connects late, avoid registering twice a
                    # connection by the same student
                    if not synced_students.get(author):
                        synced_students[author] = True
                        log(f"student {author} connected!")
                elif event_code == "terminate":
                    if synced_students.get(author):
                        del synced_students[author]
                        log(f"student {author} closed redis connection! {event_id}")
            if first_run:
                first_run = False
        except ServerConnectionClosedError:
            log("-----redis server connection closed-----")
            return

    # just as proof of concept...
    assert not synced_students

    pool.close()
    await pool.wait_closed()
    log("==== CLOSE REDIS HANDLER =====")


async def teacher():
    await teacher_progression(redis_u.STREAMS, 0, redis_u.N_STUDENTS)
    print(".......TEACHER DONE.......")


if __name__ == "__main__":
    asyncio.run(teacher())


# async def teacher_progression(
#     stream: str, whoami: int, expected_students: int, read_count: int = 5
# ):
#     def log(msg: str):
#         redis_u.timed_message(f"teacher {whoami}-{stream}: {msg}")

#     pool = await redis_u.create_redis_pool()
#     latest_ids = "$"
#     synced_students = 0
#     shut_sent = False
#     first_run = True

#     # sync teacher with messages sent by students before teacher joined
#     # log("checking for lost events")
#     # missed_events = await redis_u.sync_redis_client(pool, stream)
#     # for event_id, event in missed_events:
#     #     log(event)
#     #     author = event["emitter"]
#     #     if author == whoami:
#     #         log(f"skipping message sent by myself!")
#     #         continue
#     #     if event["payload"] == "student_connected":
#     #         log(f"student {author} connected")
#     #         synced_students += 1

#     log("start interactions!")
#     while pool:
#         try:
#             print(synced_students)
#             # once all peers have left, break
#             if not (synced_students or first_run):
#                 break
#             # once all peers have joined, tell them to leave, and start
#             # listening for events newer than this one
#             elif not shut_sent and synced_students == expected_students:
#                 term_id = await pool.xadd(
#                     stream, {"payload": "STOP", "emitter": whoami}
#                 )
#                 latest_ids = term_id
#                 shut_sent = True
#                 log(f".....sent termination message to all students: {term_id}")
#                 if first_run:
#                     first_run = False
#             log("before teacher read...")
#             events = await pool.xread(
#                 [stream], count=read_count, latest_ids=[latest_ids]
#             )
#             log("after teacher read!!!")
#             for key, event_id, event in events:
#                 log(f"key -> {key} event_id -> {event_id}")
#                 latest_ids = event_id
#                 # avoid dealing with messages sent by yourself
#                 author = int(event["emitter"])
#                 # if author == whoami:
#                 #     log(f"skipping message sent by myself!")
#                 #     continue
#                 # dispatch events
#                 event_code = event["payload"]
#                 if event_code == "terminate":
#                     log(f"student {author} closed redis connection!")
#                     synced_students -= 1
#                 elif event_code == "student_connected":
#                     log(f"student {author} connected!")
#                     synced_students += 1
#         except ServerConnectionClosedError:
#             log("-----redis server connection closed-----")
#             return

#     # just as proof of concept...
#     assert not synced_students

#     pool.close()
#     await pool.wait_closed()
#     log("==== CLOSE REDIS HANDLER =====")
