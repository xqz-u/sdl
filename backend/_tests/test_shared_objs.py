import asyncio
import json
import multiprocessing as mp
import time

import aiohttp


async def ado(session, nth):
    async with session.post("http://localhost:8000/incr-Glob") as response:
        ...
        # print(f"{nth} request! resp: {response.status}")
        # print(f"{nth} request! resp: {response.status} val: {await response.text()}")


async def ado_all(n=100):
    async with aiohttp.ClientSession() as session:
        for i in range(n):
            await ado(session, i)
        async with session.get("http://localhost:8000/Glob") as response:
            print(f"final value is: {await response.text()}")


async def atest_other(n=100):
    async with aiohttp.ClientSession() as sess:
        for i in range(n):
            async with sess.get("http://localhost:8000/test-get") as res:
                # print(f"{i}th response!")
                ...


async def test_dict(n=100):
    async with aiohttp.ClientSession() as sess:
        for i in range(n):
            d = json.dumps({i: i + 1})
            url = f"http://localhost:8000/dict-p?k={i}&v={i+1}"
            async with sess.post(url) as res:
                ...
                # print(f"posted {d}")


if __name__ == "__main__":
    start = time.time()
    # asyncio.run(ado_all(2000))
    # asyncio.run(atest_other(10000))
    asyncio.run(test_dict(10000))
    print(f"total time: {time.time() - start}")
    # do_all(2000)
    # test_other(2000)


# session = None


# def set_global_session():
#     global session
#     if not session:
#         session = requests.Session()


# def do(nth_url):
#     nth, url = nth_url
#     with session.post(url) as res:
#         ...
#         # name = mp.current_process().name
#         # print(f"{name}: {nth} req! resp: {res.content}")


# def do_all(n=100):
#     with mp.Pool(initializer=set_global_session) as p:
#         p.map(do, [(i, "http://localhost:8000/incr-Glob") for i in range(n)])
#     set_global_session()
#     with session.get("http://localhost:8000/Glob") as res:
#         print(f"final value: {res.content}")


# def test_once(i_url):
#     _, url = i_url
#     with session.get(url) as res:
#         ...


# def test_other(n=100):
#     with mp.Pool(initializer=set_global_session) as p:
#         p.map(test_once, [(i, "http://localhost:8000/test-get") for i in range(n)])
