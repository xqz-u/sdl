from backend.app.database import config as db_conf
from backend.app.database import crud
from backend.app.jatos_experiments import dataframes, figures

sess = db_conf.SessionLocal()


# national data

df_n = dataframes.national_results(sess)
df_n = dataframes.preproc_all_results(df_n)


# data relative to the following teacher
fake_teacher = crud.get_teacher(sess, "fake_teacher")

df = dataframes.class_results(sess, fake_teacher)
df = dataframes.preproc_all_results(df)

df_st_class0 = dataframes.student_results(sess, fake_teacher.students[3])
df_st_class0 = dataframes.preproc_all_results(df_st_class0)


# data relative to the following teacher
test_teacher = crud.get_teacher(sess, "test_email@gmail.com")

df1 = dataframes.class_results(sess, test_teacher)
df1 = dataframes.preproc_all_results(df1)

df_st_class1 = dataframes.student_results(sess, test_teacher.students[9])
df_st_class1 = dataframes.preproc_all_results(df_st_class1)


# grouped data first teacher
df_group = dataframes.group_results(df_st_class0, df, df_n)


# test sending tur data to render in react

# figs = figures.all_plots(df_group)

# figures.write_figures_html(figs, "/home/xqz-u/sdl/backend/app/jatos_experiments/pippo1")
