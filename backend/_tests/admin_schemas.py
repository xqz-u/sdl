from backend.app.admin import schemas as adm_sch
from backend.app.database import config as db_conf
from backend.app.database import crud
from backend.app.database import models as db_m

sess = db_conf.SessionLocal()

teacher = crud.get_teacher(sess, "rt@gmail.com")
admin = crud.get_admin(sess, "admin@gmail.com")
studA = sess.query(db_m.Student).filter_by(id=111).first()
studB = crud.get_student(sess, "1-1631205078-4")
exper = crud.get_experiment(sess, "1_second")

# all_ents = [teacher, admin, studA, studB]
all_ents = crud.get_users(sess)

conv_ents = adm_sch.convert_user_models(all_ents)


for el in conv_ents:
    print(el)
