import asyncio


class Pippo:
    def __init__(self, bg_task):
        self.bg = bg_task
        self.errors = 0

    async def show(self):
        print(self.dumb)
        print("starting bg error...")
        await asyncio.gather(self.bg)

    async def __aenter__(self):
        print("enter ctx...")
        self.dumb = 2
        return self

    # from the docs: Note that __exit__() methods should not reraise the
    # passed-in exception; this is the caller’s responsibility. :(
    async def __aexit__(self, exc_type, exc, tb):
        if exc_type:
            print(exc_type)
            self.errors += 1
        print(f"errors: {self.errors}")
        if isinstance(exc_type, ValueError):
            print("catched error in __aexit__!")
        delattr(self, "dumb")
        return True


async def sleeper(amount):
    await asyncio.sleep(amount)
    raise ValueError("some error, dummy!")


async def test():
    pippo = Pippo(asyncio.create_task(sleeper(1)))

    async with pippo as p:
        await p.show()
        raise ValueError("from body!")
        await asyncio.sleep(2)
        print("DONE pippo")

    try:
        print(pippo.a)
    except AttributeError:
        print("ERROR: pippo did not define attr 'a'!")


async def exc1(fn, arg):
    try:
        print("exec fn...")
        await fn(arg)
    except ZeroDivisionError:
        print("exc outer catched!")


async def fn(arg):
    return arg / 0


async def exc_test():
    a = 2
    t1 = asyncio.create_task(exc1(fn, a))
    el = await asyncio.gather(t1, return_exceptions=True)
    print(el)


if __name__ == "__main__":
    # asyncio.run(test())
    asyncio.run(exc_test())
