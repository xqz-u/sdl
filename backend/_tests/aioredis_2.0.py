import asyncio
from datetime import datetime

import aioredis


def timed_message(msg: str):
    print(f"{datetime.now().strftime('%H:%M:%S:%f')}: {msg}")


async def consumer(pool: aioredis.Redis):
    alive = True
    timed_message("starting consumer....")

    last_id = "$"
    for e_id, e in await pool.xrevrange("pippo"):
        timed_message(f"{e_id} -> {e}")
        if e["action"] == "die":
            timed_message("CONSUMER DIES FROM OLD MESSAGE!")
            alive = False
        last_id = e_id

    while alive:
        timed_message("wating for message...")
        resp = await pool.xread({"pippo": last_id}, block=0)
        for key, e_id, e in resp:
            timed_message("{key} -> {e_id} -> {e}")
            if e["action"] == "die":
                timed_message("CONSUMER DIES!")
                alive = False
                break


async def run():
    pool = aioredis.from_url(
        "redis://localhost", encoding="utf-8", decode_responses=True
    )
    # last_event = await pool.xadd("pippo", {"action": "die"})
    # print(f"last event: {last_event}")
    await consumer(pool)
    # await asyncio.gather(consumer(pool))
    print("DONE!")


# async def test_frompool():
#     og_client = aioredis.from_url(
#         "redis://localhost", encoding="utf-8", decode_responses=True
#     )
#     # new_client = aioredis.Redis(connection_pool=og_client.)


if __name__ == "__main__":
    asyncio.run(run())
