from datetime import datetime
from typing import Optional

import aioredis


POOL_CONFIG = (("localhost", 6379), {"encoding": "utf-8"})
N_STUDENTS = 5
STREAMS = ("rt@gmail.com:r", "rt@gmail.com:w")


def timed_message(msg: str):
    print(f"{datetime.now().strftime('%H:%M:%S:%f')}: {msg}")


async def create_redis_pool(config: tuple = POOL_CONFIG) -> aioredis.RedisConnection:
    pool = None
    address, opts = config
    try:
        pool = await aioredis.create_redis_pool(address, **opts)
    except ConnectionRefusedError as e:
        print("cannot connect to redis on:", REDIS_HOST, REDIS_PORT)
    finally:
        return pool


# when the optional parameters are not given, defaults to reading all events
# previously pushed on the stream
# NOTE could pass a callback to do the resyincing work
async def sync_redis_client(
    pool: aioredis.RedisConnection,
    stream: str,
    count: Optional[int] = None,
    start: Optional[str] = "+",
    stop: Optional[str] = "-",
) -> list:
    return await pool.xrevrange(stream=stream, count=count, start=start, stop=stop)
