import asyncio

from backend.app.database import crud
from backend.app.database import schemas as db_sch
from backend.app.database import utils as db_u
from backend.app.ws_redis import student_connector as stud_conn

# import random


async def mock_student_experiment(mng: stud_conn.StudentConnManager, current_exp: str):
    # mock_exp_time = random.randint(1, 10)
    mock_exp_time = 3
    mng.redis_log(f"do experiment {current_exp} for {mock_exp_time}s...")
    await asyncio.sleep(mock_exp_time)
    mng.redis_log(f"done with experiment {current_exp}, tell student client...")
    with db_u.Transaction() as sess:
        stud_db = crud.get_student(sess, mng.student_sch.code)
        exp_db = crud.get_experiment(sess, current_exp)
        stud_db.doing_experiment = ""
        stud_db.done_experiments.append(exp_db)
        db_u.modify_session_entry(sess, stud_db, exp_db)
        mng.student_sch = db_sch.DBStudent(stud_db)
    try:
        await mng.ws.send_json(
            {
                "action": "experiment_status",
                "payload": {"next_experiment": mng.student_sch.next_experiment},
            }
        )
    except Exception as e:
        print(f"While doing exp {current_exp}: {e}")
        print("ABORT")
    else:
        mng.redis_log("tell teacher client...")
        await mng.student_update()
        mng.redis_log("DONE MOCK EXP!")
