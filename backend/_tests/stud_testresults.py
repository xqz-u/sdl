from backend.app.database import config as db_conf
from backend.app.database import models as db_m
from backend.app.database import utils as db_u
from sqlalchemy import func
from sqlalchemy.orm import Session

sess = db_conf.SessionLocal()

exp_onesec, exp_context, exp_tur = sess.query(db_m.Experiment).all()


teacher_a, _ = sess.query(db_m.Teacher, func.min(db_m.Teacher.id)).first()
teacher_b = sess.query(db_m.Teacher).filter_by(id=2).first()

# create a couple of students
stud_a = db_m.Student(code="pippo", username="mimmo", teacher=teacher_a)
stud_b = db_m.Student(code="pippob", username="mimmob", teacher=teacher_b)

# persist them to create attributes needed by other tables
db_u.modify_session_entry(sess, stud_a, stud_b)

assert sess.query(db_m.Student).all() == [stud_a, stud_b]

# stud_a has finished first experiment
res_a_onesec = db_m.TestResult(
    practice=False,
    response_time_keyboard_exp=246,
    author_id=stud_a.id,
    experiment_id=exp_onesec.id,
)
stud_a.done_experiments.append(exp_onesec)


# stud_b did not finish first experiment
res_b_onesec = db_m.TestResult(
    practice=True,
    response_time_keyboard_exp=446,
    author_id=stud_b.id,
    experiment_id=exp_onesec.id,
)

db_u.modify_session_entry(sess, res_a_onesec, res_b_onesec)

res_a_onesec_author = (
    sess.query(db_m.Student).filter_by(id=res_a_onesec.author_id).first()
)
assert res_a_onesec_author is stud_a
assert (
    stud_a.done_experiments[0] == exp_onesec
    and stud_a.done_experiments
    == sess.query(db_m.Experiment).filter_by(name="1_second").all()
)

res_b_onesec_author = (
    sess.query(db_m.Student).filter_by(id=res_b_onesec.author_id).first()
)
assert res_b_onesec_author is stud_b
assert stud_b.done_experiments == []


# add a new test result for stud_b
res_b_onesec_2 = db_m.TestResult(
    practice=False,
    response_time_keyboard_exp=436,
    author_id=stud_b.id,
    experiment_id=exp_onesec.id,
)
db_u.modify_session_entry(sess, res_b_onesec_2)

stud_b_results = sess.query(db_m.TestResult).filter_by(author_id=stud_b.id).all()
assert stud_b_results == [res_b_onesec, res_b_onesec_2]


# give some other results to stud_a
res_context_a = db_m.TestResult(
    practice=True, response_time_reproduction=498, context="1", exp_dur="long"
)
res_context_a.author_id = stud_a.id
res_context_a.experiment_id = exp_context.id


db_u.modify_session_entry(sess, res_context_a)


# add another student to teacher_b
stud_b2 = db_m.Student(code="pippob2", username="mimmob2", teacher=teacher_b)
db_u.modify_session_entry(sess, res_context_a)

# who has a very same result for second test as the other student in the same
# class
res_b2_onesec_2 = db_m.TestResult(
    practice=False,
    response_time_keyboard_exp=436,
    author_id=stud_b2.id,
    experiment_id=exp_onesec.id,
)
db_u.modify_session_entry(sess, res_b2_onesec_2)


def experiment_results(sess: Session, experiment: db_m.Experiment) -> dict:
    return sess.query(db_m.TestResult).filter_by(experiment_id=experiment.id).all()


# TODO return pandas dataframe directly?
def student_results(sess: Session, student: db_m.Student) -> dict:
    return {
        exp.name: [
            res.to_dict()
            for res in sess.query(db_m.TestResult)
            .filter_by(experiment_id=exp.id, author_id=student.id)
            .all()
        ]
        for exp in sess.query(db_m.Experiment).all()
    }


def class_results(sess: Session, teacher: db_m.Teacher) -> list:
    return [student_results(sess, st) for st in teacher.students]


def all_results(sess: Session) -> dict:
    return {
        exp.name: [
            res.to_dict()
            for res in sess.query(db_m.TestResult).filter_by(experiment_id=exp.id).all()
        ]
        for exp in sess.query(db_m.Experiment).all()
    }
