# example to achieve global state shared across different processes (when
# running with multiple gunicorn workers)

import multiprocessing as mp
from typing import List

from fastapi import FastAPI


app = FastAPI()

# calls to routes that do not require synchronization (those not accessing g) are
# indeed faster when the app instance is run with multiple workers
g = mp.Value("i", -1)


@app.post("/incr-Glob", response_model=int)
async def incr():
    # proc = mp.current_process()
    global g
    # print(f"{proc.name},{proc.pid}: before: {g.value}")
    with g.get_lock():
        g.value += 1
    # print(f"{proc.name},{proc.pid}: now: {g.value}")
    return g.value


@app.get("/Glob", response_model=int)
async def get_Glob():
    return g.value


# however, to store a global dictionary of alive websockets, a
# multiprocessing.Manager is required; this is not a very performant solution,
# since the dict object it returns is not shared memory, rather the manager
# maintains a queue of messages in usual IPC style to update the global object.
# it acts as a server, thus it needs to listen for IPC -> kills performance
# the difference can be seen by uncommenting the following 2 lines and running
# gunicorn with multiple workers; when the third line is uncommented, you must
# use only one worker process

# manager = mp.Manager()
# d_ = manager.dict()
d_ = {}


@app.post("/dict-p", response_model=str)
async def dd(k: str, v: str):
    # proc = mp.current_process()
    # print(f"{proc.name}:{proc.pid}")
    global d_
    d_[k] = v
    return "ok"


@app.get("/dict")
async def d():
    return d
