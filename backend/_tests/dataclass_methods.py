from dataclasses import dataclass
import functools as ft

# import inspect


@dataclass
class Pippo:
    fn: callable = lambda: None
    a: int = 0

    # afn: callable = lambda: None

    def __call__(self, *args, **kwargs):
        print("calling...")
        self.fn(self.a, *args, **kwargs)
        print(self.a)
        return self


def pippo_method(fn) -> callable:
    @ft.wraps(fn)
    def inner(*args, **kwargs):
        return Pippo(fn, *args, **kwargs)

    return inner

    # @ft.wraps(fn)
    # async def ainner(*args, **kwargs):
    #     return Pippo(fn, *args, **kwargs)
    # return ainner if inspect.iscoroutinefunction(fn) else inner


@pippo_method
def fn(a):
    a = a + 1
    print("new a: ", a)


res = fn()
print(res)
res()
print(res.a)


# @dataclass
# class TeacherRedisConnector(RedisConnector):
#     active_students: dict = {}
#     full_class_ack: bool = False

#     def __post_init__(self):
#         self.streams = (f"{self.user_sch.email}:r", f"{self.user_sch.email}:w")
#         self.active_students = {code: "inactive" for code in self.user_sch.codes}


# def teacher_method(fn) -> callable:
#     @ft.wraps(fn)
#     def inner(*args, **kwargs):
#         return TeacherRedisConnector(fn, *args, **kwargs)

#     @ft.wraps(fn)
#     async def ainner(*args, **kwargs):
#         return TeacherRedisConnector(fn, *args, **kwargs)

#     return ainner if inspect.iscoroutinefunction(fn) else inner


# sync teacher client with already connected students. The `author` of a
# student message is the student's login code. Once students log in, they
# change their status to "active"; use this info instead of querying the db
# to send info to teacher client
# @teacher_method
# async def init_sync(
#     active_students: dict, latest_ids: str, ws: WebSocket, missed_events: list
# ):
#     target_event = "student_connect"
#     for event_id, event in missed_events:
#         author, event_code = u.mget(event, "author", "payload")
#         if event_code == target_event:
#             active_students[author] = "active"
#             latest_ids = event_id
#     await ws.send_json({"action": target_event, "payload": active_students})


# @teacher_method
# def log(teacher_sch: db_sch.DBTeacher, msg: str):
#     redis_u.timed_message(f"teacher {teacher_sch}: {msg}")


# @teacher_method
# async def student_connect(
#     event: dict, active_students: dict, latest_ids: str, ws: WebSocket, log: callable
# ):
#     student_status = active_students.get(author)
#     # set students status as active on their first connection or when they
#     # reconnect
#     if not student_status or student_status == "inactive":
#         active_students[author] = "active"
#         await ws.send_json({"action": event["payload"], "payload": active_students})
#         log(f"notified client, new student {author} connected!")


# @teacher_method
# async def studend_disconnect(
#     event: dict,
#     active_students: dict,
#     latest_ids: str,
#     ws: WebSocket,
#     log: callable,
#     full_class_ack: bool,
# ):
#     active_students[author] = "inactive"
#     await ws.send({"action": event["payload"], "payload": active_students})
#     full_class_ack = False
#     log(f"student {author} went offline :(")


# @teacher_method
# async def stream(
#     log: callable,
#     init_sync: callable,
#     pool: aioredis.RedisConnection,
#     streams: tuple,
#     latest_ids: str,
#     active_students: dict,
#     full_class_ack: bool,
#     ws_connected: bool,
# ):
#     # students could have joined the 'room' before teacher (e.g. teacher
#     # disconnection error); in this case, sync the teacher's status, and
#     # update latest_ids if needed
#     log("checking for lost events...")
#     self.init_sync()

#     log(f"start interactions! already in: {active_students}")
#     while pool and ws_connected:
#         try:
#             # once all peers have joined, notify teacher client
#             if not full_class_ack and all(
#                 map(lambda status: status == "active", active_studs.values())
#             ):
#                 await ws.send_json({"action": "full_class_ack", "payload": None})
#                 full_class_ack = True
#                 log("sent ack to teacher client, all students in!")
#             log(f"before teacher reads from event: {latest_ids}...")
#             events = await pool.xread(
#                 [streams[0]],
#                 count=redis_cfg.XREAD_COUNT,
#                 latest_ids=[latest_ids],
#             )
#             for key, e_id, e in events:
#                 # dispatch events, and update latest_ids depending
#                 # whether the dispatched function did or didn't
#                 latest_ids = (
#                     await getattr(self, [e["payload"]])(
#                         {"key": key, "id": e_id, "fields": e}, e["author"]
#                     )
#                     or e_id
#                 )
#         except WebSocketDisconnect:
#             log("ws disconnected!")
#             ws_connected = False
#         except RedisConnectionClosedError:
#             log("-----redis server connection closed-----")
#             ws_connected = False
