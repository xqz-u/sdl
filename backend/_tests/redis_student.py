import asyncio
import random

from aioredis.errors import ConnectionClosedError as ServerConnectionClosedError

from . import redis_common_utils as redis_u


async def student(streams: tuple, whoami: int, read_count: int = 5):
    def log(msg: str):
        redis_u.timed_message(f"student {whoami}: {msg}")

    pool = await redis_u.create_redis_pool()
    rstream, wstream = streams
    termination_received = False

    # wait some time before joining...
    delay = random.randint(1, 2)
    await asyncio.sleep(delay)
    # students start by telling teacher that they are now active
    latest_ids = await pool.xadd(
        wstream, {"payload": "student_connected", "emitter": whoami}
    )
    log(f"joined teacher after {delay}s: {latest_ids}")

    while pool:
        try:
            if termination_received:
                log(f"terminated, latest event saved: {latest_ids}")
                break
            log(f"before student {whoami} read starting from {latest_ids}...")
            events = await pool.xread(
                [rstream], count=read_count, latest_ids=[latest_ids]
            )
            for _, event_id, event in events:
                latest_ids = event_id
                author = int(event["emitter"])
                # dispatch events
                event_code = event["payload"]
                # tell master that you're dead
                if event_code == "STOP":
                    if not termination_received:
                        log(f"termination message received as: {event_id}")
                        termination_received = True
                    last_e = await pool.xadd(
                        wstream, {"payload": "terminate", "emitter": whoami}
                    )
                    log(f"teacher {author} told me to shut and I said OK: {last_e}!")
                    break
        except ServerConnectionClosedError:
            log("-----redis server connection closed-----")
            return

    pool.close()
    await pool.wait_closed()
    log("==== CLOSE REDIS HANDLER =====")


async def students():
    student_tasks = [
        asyncio.create_task(student(redis_u.STREAMS[::-1], i))
        for i in range(1, redis_u.N_STUDENTS + 1)
    ]
    _, pending = await asyncio.wait(student_tasks)
    for p_task in pending:
        print(f"canceling pending task: {p_task}")
        p_task.cancel()
    print(".......STUDENTS DONE.......")


if __name__ == "__main__":
    asyncio.run(students())


# FIXME the problem seems to be in the coordination that (does not) happen once
# the termination message is first received: by the time latest_ids is updated
# to read events from the termination event, it is too late already, and the
# other listeners might already be reading starting from the first student's
# termination time. Generally, it seems like a bad idea to coordinate
# latest_ids in this way, since there is no insurance in the operations order
# async def student(stream: str, whoami: int, read_count: int = 5):
#     def log(msg: str):
#         redis_u.timed_message(f"student {whoami}-{stream}: {msg}")

#     pool = await redis_u.create_redis_pool()
#     latest_ids = "$"
#     termination_received = False

#     # sync student with messages sent by students before teacher joined
#     # (unnecessary for this example, proof of concept)
#     # log("checking for lost events")
#     # missed_events = await redis_u.sync_redis_client(pool, stream)
#     # for event_id, event in missed_events:
#     #     author = event["emitter"]
#     #     if author == whoami:
#     #         log(f"skipping message sent by myself!")
#     #         continue
#     #     log(event)

#     # students start by telling teacher that they are now active; this
#     # is necessary or the teacher and students loop will block
#     # wait some time before joining...
#     delay = random.randint(1, 2)
#     time.sleep(delay)
#     join_id = await pool.xadd(
#         stream, {"payload": "student_connected", "emitter": whoami}
#     )
#     log(f"joined teacher after {delay}s: {join_id}")

#     while pool:
#         try:
#             log(f"before student {whoami} read...")
#             events = await pool.xread(
#                 [stream], count=read_count, latest_ids=[latest_ids]
#             )
#             log(f"after student {whoami} read!!!")
#             for key, event_id, event in events:
#                 log(f"key -> {key} event_id -> {event_id}")
#                 latest_ids = event_id
#                 # avoid dealing with messages sent by yourself
#                 author = int(event["emitter"])
#                 # if author == whoami:
#                 #     log("skipping message sent by myself!")
#                 #     continue
#                 # dispatch events
#                 event_code = event["payload"]
#                 # tell master that you're dead, and implicitly tell other
#                 # students to start inspecting events from this one
#                 if event_code == "STOP":
#                     if not termination_received:
#                         log(f"first termination message received! id: {event_id}")
#                         termination_received = True

#                     # await pool.xadd(stream, {"payload": "terminate", "emitter": whoami})
#                     log(f"teacher {author} told me to shut and I said OK!")
#                     break
#             if termination_received:
#                 await pool.xadd(stream, {"payload": "terminate", "emitter": whoami})
#                 log(f"terminated and set latest ids to: {latest_ids}")
#                 break
#         except ServerConnectionClosedError:
#             log("-----redis server connection closed-----")
#             return

#     pool.close()
#     await pool.wait_closed()
#     log("==== CLOSE REDIS HANDLER =====")
