#! /usr/bin/env python
import asyncio
from pprint import pp

import aioredis
from aioredis.errors import ConnectionClosedError as ServerConnectionClosedError

REDIS_HOST = "localhost"
REDIS_PORT = 6379


async def create_redis_connection() -> aioredis.RedisConnection:
    pool = None
    try:
        pool = await aioredis.create_redis((REDIS_HOST, REDIS_PORT), encoding="utf-8")
    except ConnectionRefusedError as e:
        print("cannot connect to redis on:", REDIS_HOST, REDIS_PORT)
    finally:
        return pool


async def listen(stream_id: str, whoami: int):
    redis_client = await create_redis_connection()
    latest_id = "$"
    first_time = True
    print(f"{stream_id}-{whoami}: connected: {redis_client}")
    while redis_client:
        try:
            if first_time:
                events = await redis_client.xrevrange(
                    stream=f"{stream_id}:stream", count=10
                )
                first_time = False
                print(f"{stream_id}-{whoami}: first time, events:")
                for event_id, fields in events:
                    print(event_id)
                    pp(fields)
            else:
                events = await redis_client.xread(
                    streams=[f"{stream_id}:stream"], count=5, latest_ids=[latest_id]
                )
                print(f"{stream_id}-{whoami}: events:")
                for key, event_id, fields in events:
                    print(key, event_id)
                    pp(fields)
                    latest_id = event_id
        except ServerConnectionClosedError:
            print("-----redis server connection closed----")
            return
    client.close()
    await client.wait_closed()


async def push_event(client: aioredis.RedisConnection, stream_id: str, **kwargs):
    res = await client.xadd(stream=f"{stream_id}:stream", **kwargs)
    print(f"xadd: {res}")


# create 2 publisher
# create 3 subscribers for each publisher
# push some events
# let subscribers read them
# end connection
async def notify_clients(streams=["pippo", "ciccio"], n_subscribers=3):
    clients = [await create_redis_connection() for _ in range(len(streams))]
    for client, stream in zip(clients, streams):
        await push_event(
            client,
            stream,
            **{"fields": {"action": "HELLLO"}, "message_id": b"*", "max_len": 1000},
        )
        print(f"pushed to stream {stream}...")
    subscribers = [
        asyncio.create_task(listen(s, n)) for s in streams for n in range(n_subscribers)
    ]
    print("launching subscribers...")
    _, pending = await asyncio.wait(subscribers)
    for p in pending:
        print(p)
        p.cancel()
    print("=======DONE=======")


async def run():
    from . import (
        redis_student as r_stud,
        redis_teacher as r_teach,
        redis_common_utils as redis_u,
    )

    teacher_task = asyncio.create_task(
        r_teach.teacher_progression(redis_u.STREAMS, 0, redis_u.N_STUDENTS)
    )
    student_tasks = [
        asyncio.create_task(r_stud.student(redis_u.STREAMS[::-1], i))
        for i in range(1, redis_u.N_STUDENTS + 1)
    ]
    _, pending = await asyncio.wait(student_tasks)
    for p_task in pending:
        print(f"canceling pending task: {p_task}")
        p_task.cancel()
    print(".......STUDENTS DONE.......")


if __name__ == "__main__":
    # asyncio.run(notify_clients())
    asyncio.run(run())
