#! /bin/bash

echo "Running migrations..."

alembic upgrade head

if [ $1 == "t" ]
then
    echo "Populating the database as defined in sdl/backend/app/populate.py..."
    python -m app.populate
fi

echo "DONE prestart.sh"
