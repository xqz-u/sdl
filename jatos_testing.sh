#!/bin/bash

# start with the default in-memory db if username or password not provided
if [ -z "$1" ] || [ -z "$2" ]; then
      ./jatos/loader.sh start
else
      user=$1
      pswd=$2
      echo "Connecting Jatos to MySQL (default port: 3306) with user: $user and password: $pswd"
      ./jatos/loader.sh start \
                        -DJATOS_DB_URL='jdbc:mysql://127.0.0.1:3306/jatos?characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC' \
                        -DJATOS_DB_USERNAME="$user" \
                        -DJATOS_DB_PASSWORD="$pswd" \
                        -DJATOS_DB_DRIVER=com.mysql.cj.jdbc.Driver
fi
