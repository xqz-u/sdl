
# Table of Contents

1.  [Jatos on MySQL](#org8419c43)
2.  [Jatos](#orgc6141ce)


<a id="org8419c43"></a>

# Jatos on MySQL

Jatos normally uses H2, an in-memory database, but it also works with MySQL and
its open source fork Mariadb. To
work with Jatos and MySQL, you either need access to the study data already
existing on a MySQL instance, or you need to create a new one. Since the data on
the existent database is all mocked, you can follow the steps listed [here](https://www.jatos.org/JATOS-with-MySQL.html) to
create a new MySQL db suitable for Jatos use.
**NOTE** once the database is deployed on our server, the password and username
should be noted down for the admin.


<a id="orgc6141ce"></a>

# Jatos

1.  Download the latest stable version of [Jatos](https://github.com/JATOS/JATOS/releases/)
2.  Unzip it in the root of the `sdl` project
3.  Check that everything works by running
    
        ./jatos_testing.sh {USERNAME} {PASSWORD}
    
    with the username and password chosen in the previous section.
    A new instance of Jatos should open in your browser on port 9000. To check
    that you are effectively using MySQL, go to the Administration > System Info
    tab and check that the URLs do match. **NOTE** if you are prompted for username
    and password at the Jatos login, they are both `admin`. If you change this
    make sure to make it accessible to the admin
4.  Download the file `sdl_osexp_experiments.zip` from the Scholierenacademie&rsquo;s
    Google Drive (it should be under Scholierenacademie > Lespakketten en MOOCs >
    Hoe Lang Duurt Saai?). In the same folder there is the
    `osexp_experiments.zip`, the basis OpenSesame experiments, they are there for
    reference. The `sdl_*` version of this archive contains the modifications to
    the original experiments needed to work with the portal; you can see them
    e.g. in `/jatos.js`
5.  Unzip the OS experiments; then, from the Jatos main page, press Import Study
    and import the three OpenSesame experiments you just extracted.
6.  Jatos can now run the OS studies, but they need to be distributed through
    some link. To do so, we need to enable a *General Multiple Worker* for each
    study, which will take care of running the study when requested (more
    information on [worker types](https://www.jatos.org/Worker-Types.html) and on [batch managers](https://www.jatos.org/Run-your-Study-with-Worker-and-Batch-Manager.html)).
    For each study:
    
    -   Go under Worker & Batch Manager > {STUDY-ID} and enable the General
        Multiple Worker
    -   Click on &rsquo;Get Link&rsquo;, and copy the given URL
    
    **NOTE** each time you want to use a new batch manager or a new worker type for an
     experiment and integrate it with the portal&rsquo;s functionality, you should modify
     the URLs in the file `/backend/jatos_config.json`, a version of which is in VC.
     It has the following structure:
    
        {
            "1_second":
            {
                "url": "http://localhost:9000/publix/1/start?batchId=1&generalMultiple",
                "relevantFields": "practice,response_time_keyboard_resp"
            },
            "another_study":
            {
                ...
            },
            ...
        }
    
    The content of this whole file is sent to the user on each login, such that
    they know where to find the experiments and their URLs are easy to modify.

