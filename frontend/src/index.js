import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import { AuthProvider } from "./utils/auth-context";
import { WebSocketProvider } from "./utils/ws-context";
import App from "./App";

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <AuthProvider>
        <WebSocketProvider>
          <App />
        </WebSocketProvider>
      </AuthProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
