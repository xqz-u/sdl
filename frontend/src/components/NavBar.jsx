import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import LoginControl from "./LoginControl";

const NavBar = () => {
  return (
    <AppBar position="static" color="primary">
      <Toolbar>
        <Box m={2} flexGrow={1}>
          <Typography variant="h6" style={{ textAlign: "center" }}>
            Hoe lang duurt saai?
          </Typography>
        </Box>
        <LoginControl />
      </Toolbar>
    </AppBar>
  );
};
export default NavBar;
