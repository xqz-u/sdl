import { useEffect, useState, useRef } from "react";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";

const CodesModal = ({ studentsInfo }) => {
  const [open, setOpen] = useState(false);
  const descriptionElementRef = useRef(null);

  useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);

  const downloadStudentCodes = () => {
    const doc = new jsPDF();
    autoTable(doc, { html: "#student-codes-table" });
    doc.save("student-codes.pdf");
  };

  return (
    <div>
      <Button color="primary" variant="contained" onClick={() => setOpen(true)}>
        Show codes...
      </Button>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        scroll="paper"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
        fullWidth
      >
        <DialogContent dividers>
          <TableContainer component={Paper}>
            <Table style={{ minWidth: 500 }} id="student-codes-table">
              <TableHead>
                <TableRow>
                  <TableCell>Code</TableCell>
                  <TableCell>Username</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {studentsInfo.map(([code, username]) => (
                  <TableRow key={code}>
                    <TableCell>{code}</TableCell>
                    <TableCell>{username}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </DialogContent>
        {studentsInfo.length > 0 && (
          <DialogActions>
            <Button onClick={downloadStudentCodes} color="primary">
              Download PDF
            </Button>
          </DialogActions>
        )}
      </Dialog>
    </div>
  );
};

export default CodesModal;
