import Plot from "react-plotly.js";

const Plot1Second = ({ plotData, plotCls, svgID }) => (
  <div className={plotCls}>
    <div className="description">
      <h1>1 Seconde Taak</h1>
      <p>
        Hieronder zie je de resultaten van het eerste experiment, de 1-seconde
        taak. In de 1-seconde taak moest je zo precies mogelijk een seconde
        inschatten. Elk punt in de grafiek staat voor een tijdswaarneming. De
        horizontale, zwarte lijnen zijn de gemiddelde scores in elke groep. De
        vragen in het lespakket helpen je de resultaten van deze grafiek te
        interpreteren.
      </p>
    </div>
    <Plot
      useResizeHandler
      style={{ width: "100%", height: "81%" }}
      divId={svgID}
      data={[
        {
          x: plotData[0].x,
          y: plotData[0].y,
          type: "violin",
          box: {
            visible: true,
          },
          meanline: {
            visible: true,
          },
          points: "all",
          line: { color: "blue" },
        },
      ]}
      layout={{
        autosize: true,
        margin: { t: 10 },
        font: { size: 16 },
        xaxis: {
          title: "Groep",
          linecolor: "#d9d9d9",
          mirror: true,
        },
        yaxis: {
          title: "Reactietijd (ms)",
          linecolor: "#d9d9d9",
          mirror: true,
          showgrid: false,
          zeroline: false,
        },
      }}
    />
  </div>
);

export default Plot1Second;
