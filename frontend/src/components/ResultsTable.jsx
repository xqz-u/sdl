import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const ResultsTable = ({ students }) => {
  const classes = useStyles();
  const hist = useHistory();

  const showStudentResults = (studCode) => {
    hist.push({
      pathname: "/results",
      state: { studCode: studCode, results: true },
    });
  };

  return (
    <>
      <Button color="primary" variant="contained">
        Download all...
      </Button>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Code</TableCell>
              <TableCell>Naam Leerling</TableCell>
              <TableCell>Results</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {Object.entries(students).map(([code, stud_info]) => (
              <TableRow key={code}>
                <TableCell>{code}</TableCell>
                <TableCell component="th" scope="row">
                  {stud_info.username}
                </TableCell>
                <TableCell>
                  <Button color="primary" variant="contained">
                    Download results
                  </Button>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => showStudentResults(code)}
                  >
                    See results
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default ResultsTable;
