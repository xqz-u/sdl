import { useAuth } from "../utils/auth-context";
import LoginButton from "./LoginButton";
import LogoutButton from "./LogoutButton";

const LoginControl = () => {
  const { user } = useAuth();

  return user ? <LogoutButton /> : <LoginButton />;
};

export default LoginControl;
