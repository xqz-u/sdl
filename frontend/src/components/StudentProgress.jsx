import { useRef } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

import { MdCheckCircle } from "react-icons/md";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: 10,
  },
  gridContainer: {
    padding: theme.spacing(2),
    border: "1px solid red",
  },
  gridElement: {
    border: "1px solid grey",
  },
}));

const expStatusColorMap = {
  done: "green",
  doing: "orange",
};

const StudentProgress = ({ student, studentStatus, experiments }) => {
  const studentCodeSpaceRef = useRef(3);
  const nExperiments = useRef(Object(experiments).length);
  const classes = useStyles();

  console.log("RENDER STUDENT PROGRESS BAR");

  const computeStatusColor = (exp, idx) => {
    let expStatus = "";
    if (studentStatus.exp_info.doing === exp) {
      expStatus = "doing";
    } else if (studentStatus.exp_info.done[idx]) {
      expStatus = "done";
    }
    return expStatusColorMap[expStatus] || "white";
  };

  return (
    <Grid container className={classes.gridElement}>
      <Grid
        item
        container
        xs={studentCodeSpaceRef.current}
        className={classes.gridElement}
        direction="column"
      >
        <Grid item className={classes.gridElement}>
          {student}
        </Grid>
        <Grid item container className={classes.gridElement}>
          <Grid item className={classes.gridElement}>
            <MdCheckCircle color={studentStatus.active ? "green" : "red"} />
          </Grid>
          <Grid item className={classes.gridElement}>
            {studentStatus.username}
          </Grid>
        </Grid>
      </Grid>
      {experiments.map((exp, idx) => {
        return (
          <Grid
            item
            xs={Math.floor(
              (12 - studentCodeSpaceRef.current) / nExperiments.current
            )}
            className={classes.gridElement}
            key={exp}
            style={{ backgroundColor: computeStatusColor(exp, idx) }}
          >
            {exp}
          </Grid>
        );
      })}
    </Grid>
  );
};

export default StudentProgress;
