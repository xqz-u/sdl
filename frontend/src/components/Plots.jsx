import Plot1Second from "./Plot1Second";
import PlotContext from "./PlotContext";
import PlotTur from "./PlotTur";

const plotComponents = {
  "1_second": Plot1Second,
  // context: PlotContext,
  // tur: PlotTur,
};

const PlotChooser = ({ data, plotSpec: { plotName, prefix }, studentID }) => {
  let Plotter = plotComponents[plotName];
  return Plotter ? (
    <Plotter
      plotData={data}
      plotCls={`${prefix}${plotName}`}
      svgID={studentID}
    />
  ) : null;
};

const Plots = ({ student, plotsData }) => {
  return Object.entries(plotsData.data).map(([expName, expData]) => (
    <PlotChooser
      key={expName}
      data={expData}
      plotSpec={{ plotName: expName, prefix: plotsData.prefix }}
      studentID={`${student.prefix}${student.code}`}
    />
  ));
};

export default Plots;
