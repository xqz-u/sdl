import { Route, Redirect } from "react-router-dom";

import { useAuth } from "../utils/auth-context";

const ProtectedRoute = ({ children, ...rest }) => {
  const { userRef } = useAuth();
  // default protected route is for guests (empty string)
  const roles = (rest._roles || "").split(" ");
  // default redirect navigates to home page
  const redirectPath = rest._redirect || "/";
  delete rest._roles;
  delete rest._redirect;

  console.log(
    `PROTECTED ROUTE ${rest.path}, user: ${userRef.current}, roles: ${roles}`
  );
  console.log("No redirect? " + roles.includes(userRef.current));

  return (
    <Route {...rest}>
      {roles.includes(userRef.current) ? (
        children
      ) : (
        <Redirect to={redirectPath} />
      )}
    </Route>
  );
};

export default ProtectedRoute;
