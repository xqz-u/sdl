import Plot from "react-plotly.js";

const PlotTur = ({ plotData, plotCls, svgID }) => {
  return (
    <div className={plotCls}>
      <div className="description">
        <h1>Risico Taak</h1>
        <p>
          Hieronder vind je de resultaten van het tweede experiment: de
          risico-taak. In de risico-taak moest je ook tijdsinschatting maken,
          maar nu kon je strafpunten krijgen als je te vroeg was. De
          verschillende kleuren geven de verschillende risico-groepen aan. De
          stippen zijn de individuele tijdswaarnemingen en de horizontale zwarte
          strepen zijn weer de gemiddelde scores in de groep.
        </p>
      </div>
      <Plot
        useResizeHandler
        style={{ width: "100%", height: "81%" }}
        divId={svgID}
        data={[
          ["Straf", "blue"],
          ["Geen straf", "orange"],
        ].map(([groupName, groupColor], i) => {
          return {
            x: plotData[i].x,
            y: plotData[i].y,
            name: groupName,
            line: { color: groupColor },
            type: "violin",
            box: {
              visible: true,
            },
            meanline: {
              visible: true,
            },
            points: "all",
          };
        })}
        layout={{
          violinmode: "group",
          legend: { title: { text: "Punish" } },
          font: { size: 16 },
          xaxis: {
            title: "Groep",
            linecolor: "#d9d9d9",
            mirror: true,
          },
          yaxis: {
            title: "Reactietijd (ms)",
            linecolor: "#d9d9d9",
            mirror: true,
            showgrid: false,
            zeroline: false,
          },
          autosize: true,
          margin: { t: 10 },
        }}
      />
    </div>
  );
};

export default PlotTur;
