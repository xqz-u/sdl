import Plot from "react-plotly.js";

const PlotContext = ({ plotData, plotCls, svgID }) => {
  return (
    <div className={plotCls}>
      <div className="description">
        <h1>Context Taak</h1>
        <p>
          Hieronder staan de resultaten voor de laatste taak: de context-taak.
          In deze context-taak moest je onder verschilden omstandigheden (veel
          lange tijdsinschattingen, of veel korte tijdsinschattingen)
          tijdsinschattingen maken. Ook hier zijn de stippen de individuele
          tijdswaarnemingen en de horizontale zwarte lijnen de gemiddeldes.
        </p>
      </div>
      <Plot
        useResizeHandler
        style={{ width: "100%", height: "81%" }}
        divId={svgID}
        data={[
          ["Lang", "blue"],
          ["Kort", "orange"],
        ].map(([groupName, groupColor], i) => {
          return {
            x: plotData[i].x,
            y: plotData[i].y,
            name: groupName,
            line: { color: groupColor },
            type: "violin",
            box: {
              visible: true,
            },
            meanline: {
              visible: true,
            },
            points: "all",
          };
        })}
        layout={{
          violinmode: "group",
          legend: { title: { text: "Context" } },
          font: { size: 16 },
          xaxis: {
            title: "Groep",
            linecolor: "#d9d9d9",
            mirror: true,
          },
          yaxis: {
            title: "Reactietijd (ms)",
            linecolor: "#d9d9d9",
            mirror: true,
            showgrid: false,
            zeroline: false,
          },
          autosize: true,
          margin: { t: 10 },
        }}
      />
    </div>
  );
};

export default PlotContext;
