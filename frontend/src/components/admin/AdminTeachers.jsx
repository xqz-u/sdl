import {
  List,
  Datagrid,
  TextField,
  EmailField,
  NumberField,
} from "react-admin";

export const AdminTeachers = (props) => (
  <List title="Teacher Accounts" {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <EmailField source="email" /> <NumberField source="n_students" />
    </Datagrid>
  </List>
);
