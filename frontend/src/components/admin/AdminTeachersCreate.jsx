import { Create, SimpleForm, TextInput } from "react-admin";

export const AdminTeachersCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="email" />
      <TextInput source="password" />
    </SimpleForm>
  </Create>
);
