import {
  List,
  Datagrid,
  TextField,
  ReferenceField,
  DateField,
} from "react-admin";

export const AdminStudents = (props) => (
  <List title="Student Accounts" {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="code" />
      <TextField source="username" />
      <ReferenceField source="teacher_id" reference="admin/teachers">
        <TextField source="id" />
      </ReferenceField>
      <DateField source="doing_experiment" />
      <TextField source="done_experiments" />
    </Datagrid>
  </List>
);
