import Button from "@material-ui/core/Button";

import { useHistory } from "react-router-dom";

import { useAuth } from "../utils/auth-context";
import { hostname } from "../utils/utils";
import { useWebSocket } from "../utils/ws-context";

const LogoutButton = () => {
  const { logout } = useAuth();
  const { closeWS } = useWebSocket();
  const hist = useHistory();

  const handleLogout = async () => {
    await logout(`${hostname()}/auth/logout`);
    closeWS();
    hist.push("/");
  };

  return (
    <Button className="btn" onClick={handleLogout}>
      Outloggen{" "}
    </Button>
  );
};

export default LogoutButton;
