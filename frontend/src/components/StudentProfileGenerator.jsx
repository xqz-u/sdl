import { useState } from "react";

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";

import { useWebSocket } from "../utils/ws-context";

const StudentProfileGenerator = () => {
  const [nStudents, setnStudents] = useState("0");
  const { sendJSON } = useWebSocket();

  console.log("RENDER STUDENTS FORM");

  const createStudents = (e) => {
    e.preventDefault();
    let studentsAmount = parseInt(nStudents);
    if (studentsAmount > 0) {
      sendJSON({ action: "register_students", payload: studentsAmount });
    }
    setnStudents("0");
  };

  return (
    <form onSubmit={createStudents}>
      <Box display="inline" m={1}>
        <TextField
          type="number"
          value={nStudents}
          label="Generate or add Students"
          onChange={(e) => setnStudents(e.target.value)}
        />
      </Box>
      <Box display="inline" m={1}>
        <Button variant="contained" m={1} onClick={createStudents}>
          Submit
        </Button>
      </Box>
    </form>
  );
};

export default StudentProfileGenerator;
