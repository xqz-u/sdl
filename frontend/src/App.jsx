import { Route, Switch } from "react-router-dom";

import { useAuth } from "./utils/auth-context";

import TeacherProfile from "./views/TeacherProfile";
import StudentProfile from "./views/StudentProfile";
import AdminProfile from "./views/AdminProfile";
import LoginRegistered from "./views/LoginRegistered";
import LoginStudent from "./views/LoginStudent";
import PlotsPage from "./views/PlotsPage";
import ClassResultsPage from "./views/ClassResultsPage";

import ProtectedRoute from "./components/ProtectedRoute";
import Navbar from "./components/NavBar";

const profileViews = {
  students: StudentProfile,
  teachers: TeacherProfile,
  admins: AdminProfile,
};

const ProfileViewer = () => {
  const { user } = useAuth();
  const Profile = profileViews[user];
  return Profile ? <Profile /> : null;
};

// TODO move call to fetch number of experiments here, and then pass it to the
// component returend by ProfileViewer. I tried by setting a nExperiments ref in
// the App body once the user logs in, but assignment would fail (ref.current is
// undefined?!)

const App = () => {
  return (
    <div className="App">
      <Navbar className="navbar" />
      <div className="content">
        <Switch>
          <Route exact path="/">
            <h1>HOME</h1>
          </Route>
          <ProtectedRoute exact path="/login-registered" _redirect="/profile">
            <LoginRegistered />
          </ProtectedRoute>
          <ProtectedRoute exact path="/login-student" _redirect="/profile">
            <LoginStudent />
          </ProtectedRoute>
          <ProtectedRoute
            exact
            path="/profile"
            _roles="teachers admins students"
          >
            <ProfileViewer />
          </ProtectedRoute>
          <ProtectedRoute
            exact
            path="/results"
            _redirect="/profile"
            _roles="teachers admins students"
          >
            <PlotsPage />
          </ProtectedRoute>
          <ProtectedRoute
            exact
            path="/class-results"
            _redirect="/profile"
            _roles="teachers"
          >
            <ClassResultsPage />
          </ProtectedRoute>
        </Switch>
      </div>
    </div>
  );
};

export default App;
