import { useEffect, useState } from "react";
import { useLocation, Redirect } from "react-router-dom";

import jsPDF from "jspdf";
import "svg2pdf.js";
import { PDFDocument } from "pdf-lib";
import * as htmlToImage from "html-to-image";

import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";

import { fetchHandleErr, hostname } from "../utils/utils";
import { isEmpty } from "../utils/utils";
import Plots from "../components/Plots";

// NOTE consider saving prefixes inside state to avoid using multiple literals...
const PlotsPage = () => {
  // plotsData [0]: student code, plotsData [1] = actual plots data
  const [plotsData, setPlotsData] = useState([]);
  const { state } = useLocation();

  // TODO avod fetching on each route change! move call to fetch to
  // StudentProfile, and fire fetch if no result exist yet?
  useEffect(() => {
    const fetchPlotData = async () => {
      let studCode = "studCode" in state ? state.studCode : undefined;
      let resource = `${hostname()}/user/results/student${
        studCode ? `?code=${studCode}` : ""
      }`;
      const {
        payload: { plots_data, student },
      } = await fetchHandleErr(resource);
      setPlotsData([student, plots_data]);
    };
    if (state) {
      fetchPlotData();
    }
  }, [state]);

  // const addPagesToPDF = async (finalDoc, pagesBuffer) => {
  //   const bufferDoc = await PDFDocument.load(pagesBuffer);
  //   const pages = await finalDoc.copyPages(
  //     bufferDoc,
  //     bufferDoc.getPageIndices()
  //   );
  //   pages.forEach((page) => finalDoc.addPage(page));
  //   return finalDoc;
  // };

  const writePlot = async (plotName) => {
    const descr = document.querySelector(`.wrap_${plotName} .description`),
      plotSVG = document.querySelector(
        `.wrap_${plotName} #code_${plotsData[0]} .plot-container .svg-container svg`
      );
    if (!descr) return;
    let descrClone = descr.cloneNode(true),
      plotClone = plotSVG.cloneNode(true);
    descrClone.appendChild(plotClone);
    console.log(descrClone);
    htmlToImage
      .toPng(descrClone)
      .then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;
        document.body.appendChild(img);
      })
      .catch(function (error) {
        console.error("oops, something went wrong!", error);
      });
    // const docPlot = new jsPDF();
    // await docPlot.svg(plotSVG);
    // plotBuffer = docPlot.output("arraybuffer");
    // let pdf = await PDFDocument.create();
    // pdf = await addPagesToPDF(pdf, descrBuffer);
    // pdf = await addPagesToPDF(pdf, plotBuffer);
    // const pdfBytes = await pdf.save();
    // let file = new Blob([pdfBytes], { type: "application/pdf" });
    // var fileURL = URL.createObjectURL(file);
    // window.open(fileURL);
  };

  const handleDownload = async () =>
    await Object.keys(plotsData[1]).map((k) => writePlot(k));

  return state && "results" in state && state.results ? (
    isEmpty(plotsData) ? (
      <CircularProgress />
    ) : (
      <>
        <Button onClick={handleDownload}>DOWNLOAD</Button>
        <Plots
          student={{ code: plotsData[0], prefix: "code_" }}
          plotsData={{ data: plotsData[1], prefix: "wrap_" }}
        />
      </>
    )
  ) : (
    <Redirect to="/profile" />
  );
};

export default PlotsPage;
