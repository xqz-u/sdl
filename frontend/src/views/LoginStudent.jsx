import { useState, useRef } from "react";

import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";

import { useAuth } from "../utils/auth-context";
import { useWebSocket } from "../utils/ws-context";
import { hostname, wsHosts } from "../utils/utils";

const rug = require("random-username-generator");

// NOTE error: rug.generate is called twice on first mount
const LoginStudent = () => {
  const [code, setCode] = useState("");
  const [codeErrorMsg, setCodeErrorMsg] = useState("");
  const defaultUsername = useRef(rug.generate());
  const [username, setUsername] = useState(defaultUsername.current);
  const [usernameErrorMsg, setUsernameErrorMsg] = useState("");
  const { login, saveUser } = useAuth();
  const { openWS } = useWebSocket();

  console.log(username);
  console.log(defaultUsername.current);

  const loginAndRedirect = async () => {
    try {
      const resp = await login(
        { code: code, username: username || defaultUsername.current },
        `${hostname()}/auth/login-student`
      );
      console.log("LOGIN SUCCESS");
      openWS(`${wsHosts()}/ws`); // TODO config
      console.log("after WS open student, about to change route...");
      saveUser(resp["role"]);
    } catch (e) {
      console.error(e);
    }
  };

  const submitLogin = (e) => {
    e.preventDefault();
    // validate code
    let validCode = code.length > 0;
    // validate username: alphanumeric, capital and non, . @ - _ are all valid
    let validUsername = !!/^[a-zA-Z0-9.@\-_]*$/.exec(username);
    if (validCode && validUsername) {
      loginAndRedirect();
    }
    // update form labels
    setCodeErrorMsg(validCode ? "" : "Invalid code");
    setUsernameErrorMsg(validUsername ? "" : "Invalid username");
  };

  return (
    <div className="login">
      <form onSubmit={submitLogin}>
        <Box display="flex" flexDirection="column">
          <TextField
            type="password"
            label="Wachtwoord"
            onChange={(e) => setCode(e.target.value)}
            error={!!codeErrorMsg}
            helperText={codeErrorMsg}
          />
          <TextField
            type="text"
            label={`Username (default: ${defaultUsername.current})`}
            onChange={(e) => setUsername(e.target.value)}
            error={!!usernameErrorMsg}
            helperText={usernameErrorMsg}
          />
          <Box m={1} display="flex" justifyContent="center">
            <Button variant="contained" onClick={submitLogin}>
              Bevestigen
            </Button>
          </Box>
        </Box>
      </form>
    </div>
  );
};

export default LoginStudent;
