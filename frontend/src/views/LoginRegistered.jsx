import { useState } from "react";

import validator from "validator";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";

import { useAuth } from "../utils/auth-context";
import { useWebSocket } from "../utils/ws-context";
import { hostname, wsHosts } from "../utils/utils";

const LoginRegistered = () => {
  const [email, setEmail] = useState("");
  const [emailErrorMsg, setEmailErrorMsg] = useState("");
  const [pswd, setPswd] = useState("");
  const [pswdErrorMsg, setPswdErrorMsg] = useState("");
  const { login, saveUser } = useAuth();
  const { openWS } = useWebSocket();

  const loginAndRedirect = async () => {
    try {
      const resp = await login(
        { username: email, password: pswd },
        `${hostname()}/auth/login`
      );
      console.log("LOGIN SUCCESS");
      if (resp.role !== "admins") {
        openWS(`${wsHosts()}/ws`); // TODO config
      }
      console.log("after WS open, about to change route..");
      saveUser(resp["role"]);
    } catch (e) {
      console.error(e);
    }
  };

  const submitLogin = (e) => {
    e.preventDefault();
    // some validation
    let validEmail = validator.isEmail(email + ""),
      validPswd = pswd.length > 0;
    if (validEmail && validPswd) loginAndRedirect();
    // update form labels
    setEmailErrorMsg(validEmail ? "" : "Invalid e-mail");
    setPswdErrorMsg(validPswd ? "" : "Password field is required");
  };

  return (
    <div className="login">
      <form>
        <Box display="flex" flexDirection="column">
          <TextField
            type="email"
            label="E-mail address"
            onChange={(e) => setEmail(e.target.value)}
            error={Boolean(emailErrorMsg)}
            helperText={emailErrorMsg}
          />
          <TextField
            type="password"
            label="Wachtwoord"
            onChange={(e) => setPswd(e.target.value)}
            error={Boolean(pswdErrorMsg)}
            helperText={pswdErrorMsg}
          />
          <Box m={1} display="flex" justifyContent="center">
            <Button variant="contained" onClick={submitLogin}>
              Bevestigen
            </Button>
          </Box>
        </Box>
      </form>
    </div>
  );
};

export default LoginRegistered;
