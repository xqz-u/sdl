import { useLocation, Redirect } from "react-router-dom";

import ResultsTable from "../components/ResultsTable";

const ClassResultsPage = () => {
  const { state } = useLocation();

  return state ? (
    <ResultsTable students={state.students} />
  ) : (
    <Redirect to="/profile"></Redirect>
  );
};

export default ClassResultsPage;
