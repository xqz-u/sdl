import {
  fetchUtils,
  Admin,
  Resource,
  ListGuesser,
  EditGuesser,
} from "react-admin";
import simpleRestProvider from "ra-data-simple-rest";
import { AdminTeachers } from "../components/admin/AdminTeachers";
import { AdminStudents } from "../components/admin/AdminStudents";
import { AdminTeachersCreate } from "../components/admin/AdminTeachersCreate";

import UserIcon from "@material-ui/icons/Group";
import Dashboard from "../components/admin/Dashboard";
import { hostname } from "../utils/utils";

// import authProviderFn from "../utils/AdminAuthProvider";
// import { useAuth } from "../utils/auth-context";

// fetch does not include cookies by default, set 'credentials: include' to
// enable this
const httpClient = (url, options = {}) => {
  if (!options.headers) {
    options.headers = new Headers({
      Accept: "application/json",
    });
  }
  options.credentials = "include";
  return fetchUtils.fetchJson(url, options);
};

const dataProvider = simpleRestProvider(hostname(), httpClient);

const AdminProfile = () => {
  // const { user, logout } = useAuth();
  // const authProvider = authProviderFn(user, logout);

  // console.log(authProvider);

  return (
    <Admin
      dataProvider={dataProvider}
      /* loginPage={false} */
      /* authProvider={authProvider} */
      dashboard={Dashboard}
    >
      <Resource
        name="admin/teachers"
        list={AdminTeachers}
        create={AdminTeachersCreate}
        icon={UserIcon}
      />
      <Resource name="admin/students" list={AdminStudents} icon={UserIcon} />
    </Admin>
  );
};

export default AdminProfile;
