import { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";

import { useWebSocket } from "../utils/ws-context";
import { fetchHandleErr, hostname, makeExperimentURL } from "../utils/utils";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: 10,
  },
  gridElement: {
    border: "1px solid grey",
  },
}));

const StudentProfile = () => {
  const { sendJSON, registerHandler } = useWebSocket();
  const [currExperiment, setCurrExperiment] = useState("");
  const [results, setResults] = useState(false);
  const experiments = useRef({});
  const classes = useStyles();
  const hist = useHistory();

  console.log("RENDER STUDENT PROFILE");

  const finishedExperiments = (expName) => expName === "END";

  useEffect(() => {
    console.log("...STUDENT FETCH DATA...");
    const initialFetch = async () => {
      const { payload } = await fetchHandleErr(
        `${hostname()}/user/student-init-sync`
      );
      if (finishedExperiments(payload.next_experiment)) {
        setResults(true);
      }
      experiments.current = payload.experiments;
      setCurrExperiment(payload.next_experiment);
    };
    initialFetch();
  }, []);

  useEffect(() => {
    registerHandler("experiment_status", (msg) => {
      let nextExp = msg.payload.next_experiment;
      console.log(`experiment next: ${nextExp}!`);
      setCurrExperiment(nextExp);
      if (finishedExperiments(nextExp)) {
        setResults(true);
      }
    });
  }, [registerHandler]);

  const showExpName = () => {
    let exp_names = Object.keys(experiments.current);
    return finishedExperiments(currExperiment)
      ? exp_names[exp_names.length - 1]
      : currExperiment;
  };

  const handleExp = (e) => {
    // comment next line to serve jatos experiments
    // e.preventDefault();
    if (results) {
      e.preventDefault();
      return;
    }
    sendJSON({
      action: "experiment_status",
      payload: { doing: currExperiment },
    });
    console.log("sent start exp msg!");
  };

  const makeExpLink = () =>
    makeExperimentURL(experiments.current[currExperiment]);

  const handleResults = () => {
    hist.push({ pathname: "/results", state: { results: results } });
  };

  return !currExperiment ? (
    <CircularProgress />
  ) : (
    <Grid container spacing={1} className={classes.gridElement}>
      <Grid
        item
        container
        xs={11}
        direction="column"
        className={classes.gridElement}
      >
        <Grid item className={classes.gridElement}>
          <a
            href={makeExpLink()}
            onClick={handleExp}
            target="_blank"
            rel="noopener noreferrer"
          >
            Start experiment {showExpName()}
          </a>
        </Grid>
        {results && (
          <Grid item className={classes.gridElement}>
            <Button onClick={handleResults}>See Results</Button>
          </Grid>
        )}
      </Grid>
      <Grid
        item
        container
        xs={1}
        direction="column"
        className={classes.gridElement}
      >
        {Object.keys(experiments.current).map((exp_name) => (
          <Grid item className={classes.gridElement} key={exp_name}>
            {exp_name}
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default StudentProfile;
