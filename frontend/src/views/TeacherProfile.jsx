import { useState, useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";

import { fetchHandleErr, hostname, makeExperimentURL } from "../utils/utils";
import { useWebSocket } from "../utils/ws-context";
import StudentProfileGenerator from "../components/StudentProfileGenerator";
import CodesModal from "../components/CodesModal";
import StudentProgress from "../components/StudentProgress";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: 10,
  },
  gridElement: {
    border: "1px solid grey",
  },
}));

// STUDENTS REPRESENTATION
// {
//     student_code_1: {
//         'username': <class 'str'>,
//         'active': <class 'bool'>,
//         'exp_info': {
//              'done': <class 'list'>,
//              'doing': <class 'str'>
//         }
//     },
//     student_code_2: {
//         ...
//     },
//  ...
// }
const TeacherProfile = () => {
  const { registerHandler } = useWebSocket();
  const experiments = useRef({});
  const [students, setStudents] = useState(null);
  const classes = useStyles();
  const hist = useHistory();

  console.log("RENDER TEACHER PROF");

  // perform initial students fetch on first mount together with the experiments
  // that will be performed
  // TODO avod fetching on each route change!
  useEffect(() => {
    console.log("...TEACHER FETCH DATA...");
    const initialFetch = async () => {
      const { payload } = await fetchHandleErr(
        `${hostname()}/user/teacher-init-sync`
      );
      experiments.current = payload.experiments;
      setStudents(payload.students);
    };
    initialFetch();
  }, []);

  // register ws handlers
  useEffect(() => {
    console.log("...TEACHER WS REGISTER...");
    registerHandler("student_status", (msg) => {
      console.log("student status is...", msg["payload"]);
      updateStudents(msg.payload);
    });
    registerHandler("register_students", (msg) => updateStudents(msg.payload));
    registerHandler("experiment_results", (msg) => {
      console.log("results are ready!");
    });
  }, [registerHandler]); // useCallback dep, effect fires on first mount

  const updateStudents = (newStudents) => {
    setStudents((prev) => {
      return { ...prev, ...newStudents };
    });
  };

  const handleSeeResults = (e) => {
    e.preventDefault();
    hist.push({
      pathname: "/class-results",
      state: { students: students },
    });
  };

  return students === null ? (
    <CircularProgress />
  ) : (
    <div className={classes.root}>
      <Grid container spacing={1} className={classes.gridElement}>
        <Grid
          item
          container
          xs={8}
          md={8}
          direction="column"
          spacing={1}
          alignItems="flex-start"
          justify="center"
        >
          <Grid
            item
            container
            spacing={1}
            className={classes.gridElement}
            alignItems="flex-start"
          >
            <Grid item className={classes.gridElement}>
              <StudentProfileGenerator />
            </Grid>
            <Grid item className={classes.gridElement}>
              <CodesModal
                studentsInfo={Object.entries(students).map(
                  ([code, stud_info]) => [code, stud_info.username]
                )}
              />
            </Grid>
          </Grid>
          <Grid item container spacing={1} className={classes.gridElement}>
            <Grid item className={classes.gridElement}>
              <Button
                color="primary"
                variant="contained"
                onClick={handleSeeResults}
              >
                See results
              </Button>
            </Grid>
          </Grid>
          <Grid item container spacing={1} className={classes.gridElement}>
            <Grid item className={classes.gridElement} xs={6}>
              {placeHolder}
            </Grid>
            <Grid
              item
              container
              xs={6}
              direction="column"
              spacing={1}
              className={classes.gridElement}
            >
              {Object.entries(experiments.current).map(([key, val], idx) => (
                <Grid key={key} item className={classes.gridElement}>
                  <a
                    href={makeExperimentURL(val)}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Tryout experiment {idx + 1}: "{key}"
                  </a>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
        <Grid container item xs={5} md={4} className={classes.gridElement}>
          {[...Object.entries(students)].map(([student, status]) => (
            <StudentProgress
              key={student}
              student={student}
              studentStatus={status}
              experiments={Object.keys(experiments.current)}
            />
          ))}
        </Grid>
      </Grid>
    </div>
  );
};

const placeHolder =
  "NOTE: To produce accurate results, you should only press the 'Generate Results' button once *all* students have finished their experiments.";
export default TeacherProfile;
