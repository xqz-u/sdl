export async function fetchHandleErr(endpoint, opts) {
  const response = await fetch(endpoint, { ...opts, credentials: "include" });
  if (response.status === 500) {
    throw new Error("Internal server error");
  }
  const data = await response.json();
  if (response.status > 400 && response.status < 500) {
    if (data.detail) {
      throw data.detail;
    }
    throw data;
  }
  return data;
}

export function isEmpty(obj) {
  return Object.keys(obj).length === 0;
}

export function makeExperimentURL(expConfig) {
  return (
    expConfig && `${expConfig.url}&relevantFields=${expConfig.relevantFields}`
  );
}

// TODO map localhost to dev and server to prod, adapt dockerfile
export function hostname() {
  let hosts_conf = {
    development: `http://${process.env.REACT_APP_BACKEND_HOST}:${process.env.REACT_APP_BACKEND_PORT}`,
  };
  console.log(hosts_conf);
  console.log(process.env.NODE_ENV);
  return hosts_conf[process.env.NODE_ENV];
}

// TODO merge with hostname
export function wsHosts() {
    let hosts_conf = {
        development: `ws:${process.env.REACT_APP_BACKEND_HOST}:${process.env.REACT_APP_BACKEND_PORT}`,
    };
    console.log(hosts_conf);
    console.log(process.env.NODE_ENV);
    return hosts_conf[process.env.NODE_ENV];
}
