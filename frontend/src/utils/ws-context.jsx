import {
  createContext,
  useCallback,
  useContext,
  // useEffect,
  useRef,
} from "react";

const WebSocketContext = createContext({});

// NOTE now useCallback should be unnecessary, since the websocket context will
// mount the first time and then no more, all updates to the websocket ref do not
// cause re-renders in fact
// TODO handle websocket reconnection on page refresh!
const WebSocketProvider = (props) => {
  const wsRef = useRef(null),
    handlers = useRef({});

  console.log("WS_CONTEXT_RENDER!!!");

  // useEffect(() => {
  //   console.log(`WS EFFECT -> ${wsRef.current}`);
  //   if (!wsRef.current) {
  //     console.log("REOPENING WS...");
  //     openWS("ws://localhost:8000/ws");
  //   }
  // });

  const registerHandler = useCallback((messageType, handlerFn) => {
    if (handlers.current[messageType]) {
      console.log(`${messageType} already has a handler defined!`);
      return;
    }
    handlers.current[messageType] = handlerFn;
    console.log(`registered handler for message type: ${messageType}`);
  }, []);

  const openWS = useCallback((endpoint) => {
    console.log("..... OPEN NEW WS .......");
    let client = new WebSocket(endpoint);
    console.log("created ws instance");
    client.onopen = () => console.log("WS OPEN!");
    client.onclose = () => console.log("=========WS CLOSE!========");
    client.onerror = (e) => console.error(`WS ERROR: ${e}`);
    client.onmessage = (e) => {
      let message = JSON.parse(e.data);
      let action = message["action"];
      if (!handlers.current[action]) {
        console.log(`No handler defined for action: ${action}`);
        return;
      }
      return handlers.current[action](message);
    };
    wsRef.current = client;
  }, []);

  const closeWS = useCallback(() => {
    if (!wsRef.current) return;
    handlers.current = {};
    wsRef.current.close();
    wsRef.current = null;
    console.log("WS ======= EVERYTHING CLOSED");
  }, []);

  const sendJSON = useCallback((msgObj) => {
    if (!wsRef.current) {
      console.log(`CANNOT SEND! WS is ${wsRef.current}`);
      return;
    }
    const wsStatus = wsRef.current.readyState;
    if (wsStatus === 0) {
      console.log("NO SEND, WS IS CONNECTING...");
      return setTimeout(sendJSON, 400, msgObj);
    }
    wsRef.current.send(JSON.stringify(msgObj));
  }, []);

  const wsContextValue = {
    openWS,
    closeWS,
    registerHandler,
    sendJSON,
  };

  return <WebSocketContext.Provider value={wsContextValue} {...props} />;
};

const useWebSocket = () => useContext(WebSocketContext);

export { WebSocketProvider, useWebSocket };
