const authProviderFn = ({ user, logout }) => {

  this.logout = logout;
  this.user = user;

  return {
    // authentication
    login: ({ username, password }) => {
      console.log("attempting login...");
      return Promise.resolve();
    },
    checkError: (error) => {
      console.log(error);
    },
    checkAuth: (params) => {
      console.log(user);
      return this.user === "admins" ? Promise.resolve() : Promise.reject();
    },
    logout: async () => {
      console.log("log out... ", user, logout);
      await this.logout("http://localhost:8000/auth/logout");
      return Promise.resolve("/");
    },
    getIdentity: () => Promise.resolve(),
    // authorization
    getPermissions: (params) => Promise.resolve(),
  };
};

export default authProviderFn;
