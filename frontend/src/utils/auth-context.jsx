import {
  createContext,
  useCallback,
  useContext,
  useRef,
  useState,
} from "react";

import { fetchHandleErr } from "./utils";

const AuthContext = createContext({});

// TODO save a user identifier (email/student code)? can be used e.g. to produce
// a stamp on the student codes pdf

// NOTE use userRef when you just need user role info, user and setUser when you
// want to cause a render
const AuthProvider = (props) => {
  const userRef = useRef(sessionStorage.getItem("role") || "");
  const [user, setUser] = useState(userRef.current);

  async function login(formData, route) {
    const opts = {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams(formData).toString(),
    };
    const data = await fetchHandleErr(route, opts);
    userRef.current = data["role"];
    return data;
  }

  const saveUser = useCallback((role) => {
    setUser(role);
    // save role info for persistence across refresh
    sessionStorage.setItem("role", role);
  }, []);

  async function logout(route) {
    const data = await fetchHandleErr(route, {
      method: "DELETE",
    });
    userRef.current = "";
    setUser("");
    sessionStorage.removeItem("role");
    return data;
  }

  // TODO memoize?
  const authContextValue = {
    userRef,
    user,
    saveUser,
    login,
    logout,
  };

  return <AuthContext.Provider value={authContextValue} {...props} />;
};

const useAuth = () => useContext(AuthContext);

export { AuthProvider, useAuth };
