#!/usr/bin/env bash

# TODO this works, but it should be integrated with MySQL on Docker as well...

# execute this script with sudo
# --net=host to use local mariadb/mysql db, but unadvised and unsafe
docker run \
    -p 9000:9000 \
    -e JATOS_DB_URL='jdbc:mysql://127.0.0.1:3306/jatos?characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC' \
    -e JATOS_DB_USERNAME=jatos-sdl \
    -e JATOS_DB_PASSWORD=saaiduurtlang \
    -e JATOS_DB_DRIVER=com.mysql.cj.jdbc.Driver \
    jatos/jatos
